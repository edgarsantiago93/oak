<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/









// Navigation Routes



Route::get('/', 'RoutingController@main')->name('main');
Route::get('/contacto', 'RoutingController@contacto')->name('contacto');
Route::get('/avances', 'RoutingController@avances')->name('avances');
Route::get('/proyecto', 'RoutingController@proyecto')->name('proyecto');
Route::get('/seguridad', 'RoutingController@seguridad')->name('seguridad');
Route::get('/recorrido', 'RoutingController@recorrido')->name('recorrido');
Route::get('/departamentos', 'RoutingController@departamentos')->name('departamentos');
Route::get('/amenidades', 'RoutingController@amenidades')->name('amenidades');
Route::get('/ubicacion', 'RoutingController@ubicacion')->name('ubicacion');
Route::get('/media', 'RoutingController@media')->name('media');

Route::get('/dwBook', 'RoutingController@downloadBook')->name('dwBook');
Route::get('/dwPdf', 'RoutingController@downloadPdf')->name('dwPdf');





Route::get('/mailtest', 'RoutingController@mailTest');




// Final Routes



Route::group(['prefix'=>'admin','middleware'=>'auth'], function () {
    Route::get('/', function () {
        return redirect()->route('admin.forms');
    });
    Route::get('/ajustes', 'AdminController@settingsRoute')->name('admin.settings');
    Route::get('/formularios', 'AdminController@formsRoute')->name('admin.forms');


    Route::get('/blog', 'AdminController@blogRoute')->name('admin.blog');
    Route::get('/blog/nuevo', 'AdminController@createBlog')->name('admin.create_post');
    Route::get('/blog/{uuid}', 'AdminController@readBlog')->name('admin.read_post');


    Route::get('/media', 'AdminController@mediaRoute')->name('admin.media');
    Route::get('/media/nuevo', 'AdminController@createMedia')->name('admin.create_media');
    Route::get('/media/{uuid}', 'AdminController@readMedia')->name('admin.read_media');


    Route::get('/avances', 'AdminController@updatesRoute')->name('admin.updates');
    Route::get('/avances/nuevo', 'AdminController@createUpdate')->name('admin.create_update');
    Route::get('/avances/{uuid}', 'AdminController@readUpdate')->name('admin.read_update');


    Route::get('/banners', 'AdminController@bannersRoute')->name('admin.banners');
    Route::get('/banners/nuevo', 'AdminController@createBanner')->name('admin.create_banner');
    Route::get('/banners/{uuid}', 'AdminController@readBanner')->name('admin.read_banner');


    Route::get('/mensajes', 'AdminController@messagesRoute')->name('admin.messages');
    // Route::get('/banners/nuevo', 'AdminController@createBanner')->name('admin.create_banner');
    Route::get('/mensajes/{uuid}', 'AdminController@readMessage')->name('admin.read_message');
});












// API

Route::post("/s/form", "AdminController@saveForm")->name("saveForm");
Route::post("/sg", "RoutingController@gi")->name("getinfo");
Route::get("/u", "AdminController@u")->name("getinfo_");
Route::post("/s/settings", "AdminController@saveSettings")->name("saveSettings");


Route::post('/s/blog', 'AdminController@api_createBlogPost')->name('saveBlog');
Route::post('/e/blog', 'AdminController@api_editBlog')->name('editBlog');
Route::post('/e/s/blog', 'AdminController@api_changeStatusBlog')->name('changeStatusBlog');

Route::post('/s/media', 'AdminController@api_createMedia')->name('saveMedia');
Route::post('/e/media', 'AdminController@api_editMedia')->name('editMedia');
Route::post('/e/s/media', 'AdminController@api_changeStatusMedia')->name('changeStatusMedia');

Route::post('/s/update', 'AdminController@api_createUpdate')->name('saveUpdate');
Route::post('/e/update', 'AdminController@api_editUpdate')->name('editUpdate');
Route::post('/e/s/update', 'AdminController@api_changeStatusUpdate')->name('changeStatusUpdate');



Route::post('/s/banner', 'AdminController@api_createBanner')->name('saveBanner');
Route::post('/e/banner', 'AdminController@api_editBanner')->name('editBanner');
Route::post('/e/s/banner', 'AdminController@api_changeStatusBanner')->name('changeStatusBanner');

Route::post("/s/form-input", "AdminController@api_saveFormInput")->name("saveFormInput");

Route::post('/e/response', 'AdminController@api_editResponse')->name('editResponse');
Route::post('/e/s/response', 'AdminController@api_changeStatusResponse')->name('changeStatusResponse');


// Image routes

Route::post('image/upload/store', 'ImageUploadController@fileStore');
Route::post('image/delete', 'ImageUploadController@fileDestroy');
Route::post('image/deleteById', 'ImageUploadController@fileDestroyById')->name('deleteImage');

// Auth::routes();

Route::get('/logout', function () {
    Auth::logout();
    return redirect('/');
});

Auth::routes();

// Route::get('/register', function () {
//     return redirect('/');
// });


// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
