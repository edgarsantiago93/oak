@extends('mainlayout')



@section('page_content')



<div class="offset_container">


    <div class="dark_section">

        <div class="container">
            <div class="title">
                DEPARTAMENTOS
            </div>
        </div>

    </div>

    <div class="container-fluid no_padding">



        <div class="offet_slider_wrapper">

            <div class="pv_button no_sm" data-slider="top"> <img src="{{asset('assets/prev.svg')}}" alt="prev"></div>
            <div class="fw_button no_sm" data-slider="top"> <img src="{{asset('assets/next.svg')}}" alt="next"></div>

            <div class="offet_slider_container">
                <div class="media_slider">
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_1.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_2.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_3.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_4.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_5.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_6.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_7.jpg')}}" alt=""></div>


                </div>
            </div>

            <div class="offet_slider_container nav_slider">
                <div class="media_slider_nav">
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_1.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_2.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_3.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_4.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_5.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_6.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/apts/OAK58_7.jpg')}}" alt=""></div>
                </div>
            </div>

        </div>
    </div>
</div>




<div class="apt_elements_wrapper">
    <div class="container no_padding">
        <div class="apt_nav_slider">
            <div class="apt_nav_slide">
                <div class="title">
                    A CON TERRAZA
                </div>
                <div class="description">
                    160 m2 <br><br>
                    2 Recámaras <br>
                    2 Baños <br>
                    Sala <br>
                    Cocina integral <br>
                    Comedor <br>
                    Estudio <br>
                    Terraza
                </div>
            </div>
            <div class="apt_nav_slide">
                <div class="title">
                    A
                </div>
                <div class="description">
                    115m2<br><br>
                    2 Recámaras<br>
                    2 Baños<br>
                    Estudio<br>
                    Sala<br>
                    Comedo<br>
                    Cocina integral
                </div>
            </div>
            <div class="apt_nav_slide">
                <div class="title">
                    B
                </div>
                <div class="description">
                    180 m2<br><br>
                    3 Recámaras<br>
                    31/2 Baños<br>
                    Sala<br>
                    Comedor<br>
                    Cocina integral<br>
                    Terraza<br>
                    Cuarto de Servicio<br>
                </div>
            </div>
            <div class="apt_nav_slide">
                <div class="title">
                    C
                </div>
                <div class="description">
                    330 m2<br><br>
                    3 Recámaras<br>
                    3 1/2 Baños<br>
                    Sala<br>
                    Comedor<br>
                    Cocina integral<br>
                    Terraza<br>
                    Cuarto de Servicio
                </div>
            </div>
            <div class="apt_nav_slide">
                <div class="title">
                    D
                </div>
                <div class="description">
                    104m2 <br><br>
                    2 Recámaras<br>
                    2 Baños<br>
                    Sala<br>
                    Comedor<br>
                    Cocina integral<br>
                    Terraza
                </div>
            </div>

            <div class="apt_nav_slide">
                <div class="title">
                    E
                </div>
                <div class="description">
                    114m2<br><br>
                    2 Recámaras<br>
                    2 Baños<br>
                    Sala<br>
                    Comedor<br>
                    Cocina integral<br>
                    Terraza
                </div>
            </div>

            <div class="apt_nav_slide">
                <div class="title">
                    F
                </div>
                <div class="description">
                    103 m2 + 55 m2 de terraza<br><br>
                    2 Recámaras<br>
                    2 Baños<br>
                    Sala<br>
                    Comedor<br>
                    Cocina integral<br>
                    Terraza
                </div>
            </div>

            <div class="apt_nav_slide">
                <div class="title">
                    G
                </div>
                <div class="description">
                    172.85 m2<br><br>
                    2 Recámaras<br>
                    2 Baños<br>
                    Sala<br>
                    Comedor<br>
                    Cocina integral<br>
                    Terraza
                </div>
            </div>

            <div class="apt_nav_slide">
                <div class="title">
                    H PH
                </div>
                <div class="description">


                    283m2 <br> <br>

                    3 Recámaras <br>
                    31/2 Baños <br>
                    Sala <br>
                    Cocina integral <br>
                    Comedor <br>
                    Cuarto de Servicio <br>
                    Terraza <br>
                    Vista a la Ciudad <br>
                    Estudio
                </div>
            </div>
            <div class="apt_nav_slide">
                <div class="title">
                    H PH2
                </div>
                <div class="description">
                    220 m2<br><br>

                    3 Recámaras
                    <br>
                    31/2 Baños
                    <br>
                    Sala
                    <br>
                    Cocina integral
                    <br>
                    Comedor
                    <br>
                    Cuarto de Servicio
                    <br>
                    Terraza
                    <br>
                    Vista a la Ciudad
                    <br>
                    Estudio
                </div>
            </div>
            <div class="apt_nav_slide">
                <div class="title">
                    G
                </div>
                <div class="description">
                    180m2<br><br>

                    3 Recámaras<br>
                    <br>
                    31/2 Baños
                    <br>
                    Sala
                    <br>
                    Comedor
                    <br>
                    Cocina integral
                    <br>
                    Terraza
                    <br>
                    Cuarto de Servicio
                </div>
            </div>
            <div class="apt_nav_slide">
                <div class="title">
                    J
                </div>
                <div class="description">
                    187m2<br><br>

                    3 Recámaras
                    <br>
                    31/2 Baños
                    <br>
                    Sala
                    <br>
                    Comedor
                    <br>
                    Cocina integral
                    <br>
                    Terraza
                    <br>
                    Cuarto de Servicio
                </div>
            </div>
            <div class="apt_nav_slide">
                <div class="title">
                    K
                </div>
                <div class="description">
                    103.5 m2 <br><br>

                    2 Recámaras
                    <br>
                    2 Baños
                    <br>
                    Sala
                    <br>
                    Comedor
                    <br>
                    Cocina integral
                    <br>
                    Terraza
                </div>
            </div>




        </div>
    </div>

    <div class="container no_padding apt_models_container">

        <div class="pv_button" data-slider="bottom"> <img src="{{asset('assets/prev.svg')}}" alt="prev"></div>
        <div class="fw_button" data-slider="bottom"> <img src="{{asset('assets/next.svg')}}" alt="next"></div>

        <div class="apt_slider_wrapper">
            <div class="apt_slider_container">
                <div class="apt_media_slider">
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/a_no_terraza.png')}}" alt="">
                    </div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/a_terraza.png')}}" alt=""></div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/b.png')}}" alt=""></div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/d.png')}}" alt=""></div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/e.png')}}" alt=""></div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/i.png')}}" alt=""></div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/i.png')}}" alt=""></div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/i.png')}}" alt=""></div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/i.png')}}" alt=""></div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/i.png')}}" alt=""></div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/i.png')}}" alt=""></div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/i.png')}}" alt=""></div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/i.png')}}" alt=""></div>
                    <div class="apt_slide_instance"><img src="{{asset('assets/apts/new/i.png')}}" alt=""></div>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection


@section('extra_buttons')
<a class="btn primary_button center_all white" href="{{route('recorrido')}}">RECORRIDO</a>
@endsection





@section('page_resources')

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script>
    $(document).ready(function(){
        $('.slide_instance').click(function(){
            let c  = $(this);
            $('.media_slider').slick('slickGoTo',c.data('slick-index'));
        })

  $('.media_slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  infinite:true,
  asNavFor: '.media_slider_nav',
});


$('.media_slider_nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.media_slider',
  variableWidth: true,
  arrows:false,
infinite:true

});


  $('.apt_media_slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  infinite:true,
  asNavFor: '.apt_nav_slider',
});


$('.apt_nav_slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.apt_media_slider',
  variableWidth: false,
  arrows:false,
 infinite:true,
});



// $('.pv_button').click(()=>$('.apt_media_slider').slick('slickPrev'));

// $('.fw_button').click(()=>$('.apt_media_slider').slick('slickNext'));



$('.pv_button').click(function(){
let slider = $(this).data('slider');
if(slider =='top'){
    $('.media_slider').slick('slickPrev')
}
else{
    $('.apt_media_slider').slick('slickPrev')
}
});


$('.fw_button').click(function(){
let slider = $(this).data('slider');
if(slider =='top'){
    $('.media_slider').slick('slickNext')
}
else{
    $('.apt_media_slider').slick('slickNext')
}
});

});
</script>
@endsection