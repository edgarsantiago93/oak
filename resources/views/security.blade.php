@extends('mainlayout')

@section('page_content')


<style>
    .security {
        background-image:url('{{asset("assets/seguridad_antisismica/seguridad_antisismica.jpg")}}') !important;
    }
</style>





<div class="offset_container adjust_height_photo">
    <div class="dark_section">
        <div class="container">
            <div class="title">
                SEGURIDAD <br>

                ANTI SISMOS
            </div>
        </div>
    </div>

    <div class="container">
        <div class="virtual offset_media_container map_photo  white_bg">
            <img src="{{asset('assets/seguridad_antisismica/seguridad_antisismica.jpg')}}" alt="">
        </div>


        <div class="text_container">
            <div class="text">
                La supervisión de Luis Bozzo, nuestro calculista estructural y uno de los más
                reconocidos en todo el mundo hace que OAK58 se distinga por el uso de tecnología de
                punta contra sismos y terremotos.
            </div>
        </div>
        <div class="text_container">
            <div class="text">
                ¿Qué es un Disipador de Energía SLB?
                <br>
                <br>

                El disipador SLB (Shear Link Bozzo) para diseño sísmico, se basa en el aumento
                localizado de la ductilidad del edificio, permitiendo una reducción significativa en las
                fuerzas inducidas por un sismo de alta intensidad.
                Este diseño es el único sistema con un doble modo de disipación de energía que
                permite mayor capacidad y seguridad estructural.
                <br>​
                <br>
                Disipadores Vs. Amortiguadores
                <br><br>
                Los amortiguadores NO cambian el periodo fundamental de las estructuras y su efecto
                es proporcional a la velocidad por lo que su efectividad práctica es limitada. Los
                disipadores pueden cambiar drásticamente el periodo estructural y los disipadores SLB
                empiezan a proteger las estructuras desde desplazamientos tan bajos como solo 1mm.
                <br><br>
                ¿Entre mayor altura, un edificio se vuelve más inestable?
                <br><br>
                La respuesta es NO.
                <br>
                Desde el punto de vista sísmico, los edificios altos son más seguros frente a sismos que
                edificios de poca o media altura. Es un tema de frecuencias o periodos de vibración.
                Cuando un sismo tiene una misma frecuencia de vibración que un edificio es cuando
                causa el mayor daño.<br>
                Los rangos de frecuencia de resonancia de un sismo van de 0.2 y hasta 3.0 segundos.
                La frecuencia de vibración en los edificios es aproximadamente de 1 segundo por cada
                10 pisos. Con 58 pisos, OAK58 tendrá una frecuencia de vibración aproximada de 5.8
                segundos. Al experimentarse un sismo, un edificio como OAK58 no se percibe pues los
                rangos de resonancia o amplificación sísmica son muy diferentes, lo cual evita la
                afectación estructural del mismo.<br><br>
                ¿Quién desarrolló los cálculos estructurales de OAK58?<br><br>
                El despacho a cargo del desarrollo del proyecto de Cálculo Estructural es Luis Bozzo, el
                cual cuenta con una amplia experiencia en el sector y numerosos proyectos
                desarrollados alrededor del mundo.<br>
                El Ingeniero civil Luis Bozzo, con maestría y doctorado en la Universidad de Berkeley,
                California, ha realizado investigaciones sobre diseño y comportamiento estructural
                independientemente y con entidades Europeas, Norteamericanas y Peruanas.<br>
                Actualmente realiza trabajos de ingeniería estructural y sismorresistente en la parte de
                diseño e investigación en España, México, Perú, Bulgaria, Bahrein, Francia, Bolivia y
                Chile. Si quieres más información puedes consultar su página web www.luisbozzo.com





                <br><br>


                Para OAK58 tu seguridad es lo más importante. Es por eso que brindamos información para que tú y tu
                familia estén convencidos de que OAK58 está diseñado para resistir cualquier tipo de sismo o terremoto.
                Al igual que tu integridad física, tu inversión estará segura y resguardada en OAK58

            </div>
        </div>


    </div>
</div>





@endsection