@extends('mainlayout')

@section('page_content')



<div class="offset_container">


    <div class="dark_section">

        <div class="container">
            <div class="title two_lines">
                AVANCES <br> DE OBRA
            </div>
        </div>

    </div>


    <div class="container">




        @foreach ($updates as $update)
        @if($loop->first)

        <div class="virtual offset_media_container update_bg"
            style="background-image: url('images/{{$update->imgLink()}}')">

        </div>

        <div class="update_container">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="left_title">
                        {{$update->title}}
                    </div>
                </div>
                <div class="col-sm-12 col-md-8">
                    <div class="text_container">
                        <div class="text post_text">
                            {!!$update->content!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @else



        <div class="update_container">
            <div class="virtual offset_media_container no_offset update_bg"
                style="background-image: url('images/{{$update->imgLink()}}')">
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="left_title">
                        {{$update->title}}
                    </div>
                </div>
                <div class="col-sm-12 col-md-8">
                    <div class="text_container">
                        <div class="text post_text">
                            {!!$update->content!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endif
        @endforeach





    </div>


</div>


@endsection