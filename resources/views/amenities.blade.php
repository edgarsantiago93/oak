@extends('mainlayout')



@section('page_content')



<div class="offset_container">


    <div class="dark_section">

        <div class="container">
            <div class="title">
                AMENIDADES
            </div>
            <div class="text_container">
                <div class="text white no_b">
                    Los residentes tendrán acceso a más de <b>30 amenidades</b> con <b>acabados premium</b>
                    que les permitirán tener un estilo de vida de lujo con comodidades y servicios de
                    primer nivel dentro del mismo desarrollo. Podrán disfrutar de áreas completamente
                    equipadas y operadas por personal capacitado.
                </div>
            </div>

        </div>

    </div>






    <div class="container-fluid no_padding">

        <div class="offet_slider_wrapper" style=" margin-top: -90px;">

            <div class="pv_button no_sm" data-slider="top" style="top: 90%"> <img src="{{asset('assets/prev.svg')}}"
                    alt="prev"></div>
            <div class="fw_button no_sm" data-slider="top" style="top: 90%"> <img src="{{asset('assets/next.svg')}}"
                    alt="next"></div>

            <div class="offet_slider_container">
                <div class="media_slider">
                    <div class="slide_instance"><img src="{{asset('assets/amenities/ALBERCA TECHADA.jpg')}}" alt="">
                    </div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/ALBERCA TERRAZA DIA.jpg')}}" alt="">
                    </div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/CABANAS BBQ.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/CENTRO DE NEGOCIOS.jpg')}}" alt="">
                    </div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/CINEMA.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/FIRE PITS TERRAZA.jpg')}}" alt="">
                    </div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/GYM.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/LUDOTECA.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/PABELLON SALAS DE MASAJE.jpg')}}"
                            alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/RESTAURANT OAK 58.jpg')}}" alt="">
                    </div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/TERRAZA RESTAURANTE.jpg')}}" alt="">
                    </div>

                </div>
            </div>

            <div class="offet_slider_container nav_slider">
                <div class="media_slider_nav">
                    <div class="slide_instance"><img src="{{asset('assets/amenities/ALBERCA TECHADA.jpg')}}" alt="">
                    </div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/ALBERCA TERRAZA DIA.jpg')}}" alt="">
                    </div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/CABANAS BBQ.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/CENTRO DE NEGOCIOS.jpg')}}" alt="">
                    </div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/CINEMA.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/FIRE PITS TERRAZA.jpg')}}" alt="">
                    </div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/GYM.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/LUDOTECA.jpg')}}" alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/PABELLON SALAS DE MASAJE.jpg')}}"
                            alt=""></div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/RESTAURANT OAK 58.jpg')}}" alt="">
                    </div>
                    <div class="slide_instance"><img src="{{asset('assets/amenities/TERRAZA RESTAURANTE.jpg')}}" alt="">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="amenities third_container full_width_banner tower_cont center_all container">

    <div class="row no_padding no_margin " style="width: 100%;">
        <div class="col-6 col-md-4 center_all left_list">
            <div class="list_container">
                <div class="am_title">
                    PLANTA BAJA
                </div>





                <div class="am_list">
                    <li>Lobby Recepción</li>
                    <li>OakLink: aplicación de Servicio de concierge</li>
                    <li>Monitoreo y control de seguridad 24/7</li>
                    <li>Lobby Lounge</li>
                    <li>2 simuladores profesionales de golf</li>
                    <li>Minisuper</li>
                    <li>Servicio de tintorería, lavandería, sastrería y costura</li>
                    <li>Baños en lobby</li>
                    <li>Alberca techada semi-olímpica con vestidores y</li>
                    <li>regaderas (2 carriles de nado)</li>
                    <li>Chapoteadero techado</li>
                    <li>Baños y regaderas</li>
                    <li>Kids Club: alberca de pelotas, muro de escalar, baños y jardín</li>
                    <li>privado con arenero</li>
                    <li>Piscina exterior y espejo de agua con 5 fire pits</li>
                    <li>Área de BBQ con asadores en 2 cabañas</li>
                    <li>Área Pet Friendly</li>
                    <li>Jardines</li>
                    <li>Drop off escolar</li>
                    <li>Terraza de juegos crucero</li>
                </div>
            </div>

        </div>
        <div class="col-sm-12 col-md-4 center_all tower_img">
            <img src="{{asset('assets/amenities/torres.png')}}" alt="">
        </div>

        <div class="col-6 col-md-4 center_all right_list">


            <div class="list_container">
                <div class="am_title">
                    MEZZANINE
                </div>
                <div class="am_list">
                    Cinema
                    Karaoke
                    Night Club
                    2 Salones de eventos con cocina y baños ( 150 pax )
                    Área Lounge afuera de cada uno de los salones
                    Bussines Center: con tres privados, área de coworking, sala de juntas y
                    área de lectura
                    Teens Lounge
                </div>


                <div class="am_title">
                    PISO 27
                </div>
                <div class="am_list">
                    <li>Restaurante con cava y terraza con fire pits con Room Service.</li>
                    <li>Área para escuela de cocina</li>
                    <li>Gym: área de pesas , cardio, pilates, salón de clases, crossfit, ring de</li>
                    box, spinning, wellnes,terraza y baños
                    <li>Cancha de Paddle</li>
                    <li>Barra de jugos</li>
                </div>
                <div class="am_title">
                    PISO 28
                </div>
                <div class="am_list">
                    <li> área de relajación, jacuzzis, chorros de agua, tinas de inmersión(fría y
                        caliente), cabinas de masaje, cabinas de tratamientos, sauna, vapor</li>
                    <li> Sports Bar</li>
                </div>
            </div>




        </div>


    </div>

</div>







@endsection











@section('page_resources')

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script>
    $(document).ready(function(){
        $('.slide_instance').click(function(){
            let c  = $(this);
            $('.media_slider').slick('slickGoTo',c.data('slick-index'));
        })

  $('.media_slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  infinite:true,
  asNavFor: '.media_slider_nav',

responsive: [
    // {
    //   breakpoint: 1230,
    //   settings: {
    //     slidesToShow: 3,
    //     slidesToScroll: 3,
    //     infinite: true,
    //     dots: true
    //   }
    // },

    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]



});


$('.media_slider_nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.media_slider',
  variableWidth: true,
  arrows:false,
infinite:true

});

$('.pv_button').click(()=>$('.media_slider').slick('slickPrev'));

$('.fw_button').click(()=>$('.media_slider').slick('slickNext'));



});
</script>
@endsection