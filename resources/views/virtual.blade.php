@extends('mainlayout')

@section('page_content')



<div class="offset_container">


    <div class="dark_section">

        <div class="container">
            <div class="title two_lines">
                RECORRIDO <br> VIRTUAL
            </div>
        </div>

    </div>

    <div class="container">
        <div class="virtual offset_media_container">

            <iframe style="width:100%;min-height: 500px;" src='https://my.matterport.com/show/?m=goKPuqG5ZAY'
                frameborder='0' allowfullscreen allow='vr'></iframe>
        </div>


        <div class="text_container">
            <div class="text">
                {{-- Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                laoreet
                dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper
                suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit
                in
                vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et
                accumsan et iusto odio dignissim qui blandit --}}
            </div>

        </div>
    </div>
    <div class="container">
        <div class="virtual offset_media_container no_offset">

            <iframe style="width:100%;min-height: 500px;" src='https://my.matterport.com/show/?m=LkdEeQCCFzF'
                frameborder='0' allowfullscreen allow='vr'></iframe>


        </div>


        <div class="text_container">
            <div class="text">
                {{-- Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                laoreet
                dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper
                suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit
                in
                vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et
                accumsan et iusto odio dignissim qui blandit --}}
            </div>

        </div>
    </div>


</div>


@endsection