<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

    @php
    $yt= $settings->filter(fn($s)=>$s->type=='youtube')->first();
    $wa= $settings->filter(fn($s)=>$s->type=='whatsapp')->first();
    $ins= $settings->filter(fn($s)=>$s->type=='instagram')->first();
    $fb= $settings->filter(fn($s)=>$s->type=='facebook')->first();
    $li= $settings->filter(fn($s)=>$s->type=='linkedin')->first();
    $sp= $settings->filter(fn($s)=>$s->type=='spotify')->first();
    $tr= $settings->filter(fn($s)=>$s->type=='track_code')->first();
    $trBody= $settings->filter(fn($s)=>$s->type=='track_code_body')->first();
    $ph= $settings->filter(fn($s)=>$s->type=='phone')->first();
    @endphp


    @if (isset($tr))
    {!!$tr->content!!}
    @endif

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>OAK</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <style>

    </style>


    @if ($message = Session::get('download'))
    <meta http-equiv="refresh" content="1;url={{ route('dwBook')}}">
    @endif
</head>


<body>


    <div class="loader_wrapper">
        <div class="loader_container center_all">
            <div>
                <img src="{{asset('assets/home/logo_sm.png')}}" alt="img">
                Cargando...
            </div>
        </div>
    </div>

    @if (isset($trBody))
    {!!$trBody->content!!}
    @endif

    {{-- @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block" style="text-align: center;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @endif --}}

    @if(isset($banner))
    <div class="banner_container">
        {{$banner->content}}
    </div>
    @endif


    <div class="social_container">

        @if (isset($ph)&&$ph->content!='')
        <div class="social "> <a href="tel:{{$ph->content}}" target="_blank"><img
                    src="{{asset('assets/social/phone_.svg')}}" alt="social" style="width: 42px;">
            </a></div>
        @endif

        @if (isset($wa)&&$wa->content!='')
        <div class="social wa"> <a
                href="https://api.whatsapp.com/send?phone={{$wa->content}}&text=Hola,%20Me%20comunico%20desde%20la%20p%C3%A1gina%20web%20de%20OAK%2058."
                target="_blank"><img src="{{asset('assets/social/whatsapp.svg')}}" alt="social"> </a></div>
        @endif


        @if (isset($fb)&&$fb->content!='')
        <div class="social fb"> <a href="{{$fb->content}}" target="_blank"><img
                    src="{{asset('assets/social/facebook.svg')}}" alt="social">
            </a></div>
        @endif

        @if (isset($ins)&&$ins->content!='')
        <div class="social insta"> <a href="{{$ins->content}}" target="_blank"><img
                    src="{{asset('assets/social/_Instagram.svg')}}" alt="social"> </a></div>
        @endif


        @if (isset($yt)&&$yt->content!='')
        <div class="social yt"> <a href="{{$yt->content}}" target="_blank"><img
                    src="{{asset('assets/social/youtube.svg')}}" alt="social">
            </a></div>
        @endif


        @if (isset($li)&&$li->content!='')
        <div class="social li"> <a href="{{$li->content}}" target="_blank"><img
                    src="{{asset('assets/social/linkedin.svg')}}" alt="social">
            </a></div>
        @endif

        @if (isset($sp)&&$sp->content!='')
        <div class="social sp"> <a href="{{$sp->content}}" target="_blank"><img
                    src="{{asset('assets/social/spotify.svg')}}" alt="social">
            </a></div>
        @endif


    </div>




    <div class="sm_menu_container _no_home">
        <img src="{{asset('assets/home/logo_sm.png')}}" alt="img" class="logo_sm">
        <div class="ham_sm black ">
            <div></div>
            <div></div>
            <div></div>
        </div>

        <div class="menu_sm nohome">
            <div class="inner_menu_wrapper">
                <a href="{{route('main')}}"> Home</a>
                <a href="{{route('proyecto')}}"> Proyecto</a>
                <a href="{{route('seguridad')}}"> Seguridad Antisismos</a>
                <a href="{{route('recorrido')}}"> Recorrido Virtual</a>
                <a href="{{route('departamentos')}}"> Departamentos</a>
                <a href="{{route('amenidades')}}"> Amenidades</a>
                <a href="{{route('ubicacion')}}"> Ubicación</a>
                <a href="{{route('contacto')}}"> Contacto</a>
                <a href="{{route('avances')}}"> Avances de obra</a>
                <a href="{{route('media')}}"> Blog</a>
            </div>
        </div>

    </div>
    <div class="container-fluid no_padding">
        <div class="top_menu_bar">
            <div class="logo_container">
                <a href="{{url('/')}}"><img src="{{asset('assets/log.png')}}" alt=""></a>
            </div>
            <div class="hm_menu">
                <a href="{{url('/')}}"><img src="{{asset('assets/menu.png')}}" alt="" class="hm_menu_img"></a>
            </div>





            <div class="menu home_initial_menu">
                {{-- <a href="{{route('main')}}"> Home</a> --}}
                <a href="{{route('proyecto')}}"> Proyecto</a>
                <a href="{{route('seguridad')}}"> Seguridad Antisismos</a>
                <a href="{{route('recorrido')}}"> Recorrido Virtual</a>
                <a href="{{route('departamentos')}}"> Departamentos</a>
                <a href="{{route('amenidades')}}"> Amenidades</a>
                <a href="{{route('ubicacion')}}"> Ubicación</a>
                <a href="{{route('contacto')}}"> Contacto</a>
                <a href="{{route('avances')}}"> Avances de obra</a>
                <a href="{{route('media')}}"> Blog</a>
            </div>
        </div>


        <div class="page_content @">

            @yield('page_content')

            @if(!isset($showButtons) || (isset($showButttons) && $showButtons))
            <div class="fourth_container button_container">
                <a class="btn primary_button center_all white" id="dwBook">BROCHURE</a>
                <a class="btn primary_button center_all white" href="{{route('dwPdf')}}" id="dwPdf" target="_blank"
                    style="line-height:1!important">PRESENTACIÓN DE INVERSIONISTAS</a>
                @yield('extra_buttons')
            </div>

            @endif

        </div>

        <div class="footer_bar @if(isset($bigFooter) || isset($bigFooter)) big_footer @endif">


            <div class="text">2020 OAK 58. Creado por Building.</div>
            <div class="ap">
                <a href="">AVISO DE PRIVACIDAD</a>
            </div>

        </div>

    </div>


</body>



<div class="modal" tabindex="-1" role="dialog" id="inputModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body brochure">
                <div class="form_container container">

                    <div class="text"> Por favor ingresa tus datos para descargar el brochure</div>

                    <form action="{{route('saveFormInput')}}" method="POST" id="homeForm">
                        @csrf
                        <input type="text" name='brochure' value=true hidden>
                        <div class="row">

                            <div class="col-12">
                                <div class="input_container">
                                    <div class="label">
                                        <label for="">Nombre</label>
                                    </div>
                                    <div class="input">
                                        <input type="text" placeholder='Nombre completo' required name="input_name">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="input_container">
                                    <div class="label">
                                        <label for="">Correo</label>
                                    </div>
                                    <div class="input">
                                        <input type="email" placeholder="Correo" required name="input_mail">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="input_container">
                                    <div class="label">
                                        <label for="">Teléfono</label>
                                    </div>
                                    <div class="input">
                                        <input type="text" placeholder="Teléfono" required name="input_phone">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="form_button_container">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>

                            <div class="spacer">

                            </div>
                            <button
                                class="form_button black_button saveInputButtonn saveInputButtonnModal">ENVIAR</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>



<div class="thank_you_container  @if ($message = Session::get('success')) showTy @else hideTy @endif">

    <div class="container-fluid dark_top no_padding no_margin">
        <div class="top_dark">
            <img src="{{asset('assets/logo_h_gold.svg')}}" alt="">
        </div>
    </div>

    <div class="row center_all">
        <div class="container light_bottom no_padding no_margin">
            <div class="img">
                <img src="{{asset('assets/tyimage.png')}}" alt="">
            </div>
            <div class="text">
                ¡GRACIAS POR TU INTERÉS EN OAK58!
            </div>


            <div class="img_">
                <img src="{{asset('assets/escudo.png')}}" alt="">
            </div>
        </div>
    </div>




</div>


<script src="{{asset('/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('/js/axios.min.js')}}"></script>
<script src="{{asset('/js/bootstrap.min.js')}}"></script>
<script src="//code.tidio.co/fun0iqw7d089bqyqsxrc1qahw19dgn9x.js"></script>
<script src="//scripts.iconnode.com/82369.js"></script>
<script>
    function showLoader(){
        $('.loader_wrapper').fadeIn('fast');
    }
    function hideLoader(){
        $('.loader_wrapper').hide();
    }


    $('.ham_sm').click(function(){
        $('.menu_sm').toggleClass('menuVisible');
    })



    $('#dwBook').click(function(){
        $('#inputModal').modal();
    })


    $('.saveInputButtonnModal').click(function(e){
    let valid = true;
  $('[required]').each(function() {
    if ($(this).is(':invalid') || !$(this).val()) valid = false;
  })

  if(valid){
    $('#inputModal').modal('hide');
    showLoader();
  }
})

$(function() {
@if ($message = Session::get('success'))
    $('.thank_you_container').show();
    setTimeout(function(){
        $('.thank_you_container').fadeOut('fast');
    },5000);
@endif

});







</script>


@yield('page_resources')

</html>


{{-- facturacion@amatedigital.mx --}}