@extends('mainlayout',['showButtons'=>false])
@section('page_content')

<style>
    .slide_1 {
        background-image: url('{{asset("assets/home/1.png")}}') !important;
    }

    .slide_2 {
        background-image: url('{{asset("assets/home/2.png")}}') !important;
    }

    .slide_3 {
        background-image: url('{{asset("assets/home/3.png")}}') !important;
    }

    .slide_4 {
        background-image: url('{{asset("assets/home/4.png")}}') !important;
        margin-bottom: 20px;
    }

    .image_cont {
        background: url('assets/home/top.png');
        background-position-x: 50% !important;
        background-position-y: 100% !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }

    ._no_home {
        display: none !important;
    }

    .home_initial_menu>a {
        color: white !important;
    }


    .home_initial {
        margin-top: -150px;
    }

    @media screen and (max-width:1045px) {
        .home_initial {
            margin-top: 0px;
        }

    }

    .home_slider {
        width: 100% !important;
    }

    .home_slide_instance {
        width: 100%;
        min-height: 810px !important;
        background-size: cover !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
    }

    .top_menu_bar {
        z-index: 99;
    }

    .slick-list {
        position: absolute !important;
        width: 100% !important;
    }

    .sm_slider_wrapper {
        width: 100%;
        position: absolute !important;
        height: 100%;
        height: 500px;
    }

    .home_slider_sm {}

    .home_slide_instance_sm {
        width: 100%;
        height: 100%;
        height: 500px !important;
        background-size: cover !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
    }
</style>


<div class="initial_container home_initial">
    <div class="image_cont bg_contain">

        <div class="home_slider no_sm">
            <div class="home_slide_instance" style="background: url('{{asset('assets/home/slider/1.png')}}')"></div>
            <div class="home_slide_instance" style="background: url('{{asset('assets/home/slider/2.png')}}')"></div>
            <div class="home_slide_instance" style="background: url('{{asset('assets/home/slider/3.png')}}')"></div>
            <div class="home_slide_instance" style="background: url('{{asset('assets/home/slider/4.png')}}')"></div>
            <div class="home_slide_instance" style="background: url('{{asset('assets/home/slider/5.png')}}')"></div>

        </div>



        <div class="sm_slider_wrapper sm_show">
            <div class="home_slider_sm ">
                <div class="home_slide_instance_sm" style="background: url('{{asset('assets/home/slider/1sm_.png')}}')">
                </div>
                <div class="home_slide_instance_sm" style="background: url('{{asset('assets/home/slider/2sm_.png')}}')">
                </div>
                <div class="home_slide_instance_sm" style="background: url('{{asset('assets/home/slider/3sm_.png')}}')">
                </div>
                <div class="home_slide_instance_sm" style="background: url('{{asset('assets/home/slider/4sm_.png')}}')">
                </div>
                <div class="home_slide_instance_sm" style="background: url('{{asset('assets/home/slider/5sm_.png')}}')">
                </div>
            </div>

        </div>



        <div class="sm_menu_container">


            <div class="new_menu_container">
                <img src="{{asset('assets/home/logo_white.png')}}" alt="img" class="logo_sm" style="height: 115px;">

                <div class="ham_cont">
                    <div class="ham_sm">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>

            </div>


            <div class="menu_sm">
                <div class="inner_menu_wrapper  home_menu_wrapper">
                    {{-- <a href="{{route('main')}}"> Home</a> --}}
                    <a href="{{route('proyecto')}}"> Proyecto</a>
                    <a href="{{route('seguridad')}}"> Seguridad Antisismos</a>
                    <a href="{{route('recorrido')}}"> Recorrido Virtual</a>
                    <a href="{{route('departamentos')}}"> Departamentos</a>
                    <a href="{{route('amenidades')}}"> Amenidades</a>
                    <a href="{{route('ubicacion')}}"> Ubicación</a>
                    <a href="{{route('contacto')}}"> Contacto</a>
                    <a href="{{route('avances')}}"> Avances</a>
                    <a href="{{route('media')}}"> Blog</a>
                </div>
            </div>

        </div>


    </div>
</div>




<div class="sm_container sm_show" style="border-radius: 0px!important">
    <div class="container">
        <div class="text_container">
            <div class="text big-text sm-text">
                <div class="sm_big_text">
                    <b>OAK 58 – High Living</b>
                </div>

                <div class="sm_small_text">
                    es un desarrollo de departamentos de lujo
                    respaldado por dos empresas reconocidas a nivel internacional.
                    <br>
                    <br>
                    <b>ELÍAS ESTUDIO</b> diseña el complejo arquitectónico que supera los
                    esquemas de la vivienda vertical y <b>LUIS BOZZO</b> supervisa que la
                    estructura y los materiales cumplan con los más altos estándares
                    internacionales, logrando así, un proyecto icónico que garantiza a
                    los residentes un estilo de vida superior gracias a sus instalaciones,
                    distribución, funcionalidad, diseño, innovación e ingeniería.
                </div>
            </div>
        </div>
    </div>
</div>





<div class="project offset_container min_height_big " style="margin-bottom:0!important">
    <div class="dark_section sm_light">
        <div class="title img_title no_sm">
            <img src="{{asset('assets/escudo.png')}}" alt="">
        </div>
        <div class="container">
            <div class="text_container">
                <div class="text big-text sm-text">
                    <b>OAK 58 – High Living</b>
                    es un desarrollo de departamentos de lujo
                    respaldado por dos empresas reconocidas a nivel internacional.
                    <br>
                    <br>


                    <b>ELÍAS ESTUDIO</b> diseña el complejo arquitectónico que supera los
                    esquemas de la vivienda vertical y <b>LUIS BOZZO</b> supervisa que la
                    estructura y los materiales cumplan con los más altos estándares
                    internacionales, logrando así, un proyecto icónico que garantiza a
                    los residentes un estilo de vida superior gracias a sus instalaciones,
                    distribución, funcionalidad, diseño, innovación e ingeniería.

                </div>
            </div>
        </div>

        {{-- <div class="min_text no_sm">
            AMENIDADES
        </div> --}}
    </div>




    <div class="container">
        <div class="offset_media_container  bg_props adjust_mt yt_video_container_home ">

        </div>



        <div class="min_text sm_show _lighttext">
            AMENIDADES
        </div>
        <div class="min_text no_sm" style="    font-size: 30px;">
            AMENIDADES
        </div>

        <div class="text_container sm_no_mt">
            <div class="text black_text sm_text no_b_sm sm_no_mt sm_no_pt sm_mb no_b no_padding" style="
                margin-bottom: 30px;    font-size: 24px!important;">
                Los residentes tendrán acceso a más de 30 amenidades con acabados premium que
                les permitirán tener un estilo de vida de lujo con comodidades y servicios de primer
                nivel dentro del mismo desarrollo. Podrán disfrutar de áreas completamente equipadas
                y operadas por personal capacitado.
            </div>
        </div>


        <div class="offset_media_container no_offset slide_1 bg_props">

        </div>

        <div class="row">
            <div class="col-6 mb-4">
                <div class="offset_media_container no_offset slide_2 bg_props">
                </div>
            </div>
            <div class="col-6 mb-4">
                <div class="offset_media_container no_offset slide_3 bg_props">
                </div>

            </div>

        </div>

        <div class="offset_media_container no_offset slide_4 bg_props">

        </div>




        <div class="button_container sm_height">
            <a class="btn primary_button center_all white sm_change_bg" href="{{route('amenidades')}}">VER MÁS</a>
            @yield('extra_buttons')
        </div>



        <div class="min_text no_sm">
            UBICACIÓN
        </div>
    </div>

    <div class="sm_show grey_cont " style="justify-content: center;">
        <div class="map_wrapper center_all d-flex" style="min-height: 350px">
            <a target="_blank" href="https://g.page/oak-58---high-living?share"><img
                    src="{{asset('assets/map/map_home.png')}}" alt="" style="width: 100%;
                max-width: 500px;"></a>
        </div>

    </div>




    <div class="sm_show ubicacion">



        <div class="min_text sm_show _lighttext _light-sm">
            UN ENTORNO ÚNICO EN CERCANÍA DE LO MEJOR
        </div>



        <div class="text_container sm_no_mt">
            <div class="text black_text sm_text no_b_sm sm_no_mt sm_no_pt sm_mb no_b"
                style="font-size: 24px!important;">
                Ubicación con excelente conectividad a establecimientos de primera
                necesidad, negocios y entretenimiento. <br>
                Con dos salidas de acceso; Avenida Atlixcáyotl y Angelópolis, la zona de mayor
                categoría y plusvalía en los últimos años.
            </div>
        </div>
        <div class="button_container sm_height">
            <a class="btn primary_button center_all white sm_change_bg" href="{{route('ubicacion')}}">CÓMO LLEGAR</a>

        </div>


    </div>






    <div class="dark_wrapper_inverse no_sm">

        <div class="container rel_pos">

            <div class="grey_cont no_sm nobg">
                <a href="https://goo.gl/maps/nvppNYM11GMcwbLd7" target="_blank"> <img
                        src="{{asset('assets/map/map_home.png')}}" alt=""></a>
            </div>

            <div class="m_text">
                UN ENTORNO ÚNICO EN CERCANÍA DE LO MEJOR
            </div>
            <div class="text_container">
                <div class=" white_text text">
                    Ubicación con excelente conectividad a establecimientos de primera
                    necesidad, negocios y entretenimiento. <br>
                    Con dos salidas de acceso; Avenida Atlixcáyotl y Angelópolis, la zona de mayor
                    categoría y plusvalía en los últimos años.
                </div>
            </div>
            <div class="fourth_container button_container">
                <a target="_blank" class="btn primary_button center_all"
                    href="https://www.waze.com/live-map/directions?dir_first=no&utm_source=waze_website&utm_campaign=waze_website&utm_medium=website_menu&to=place.ChIJDYjfGiG5z4UREuOxYS7yZS0">WAZE</a>
                <a target="_blank" class="btn primary_button center_all"
                    href="https://goo.gl/maps/nvppNYM11GMcwbLd7">GOOGLE
                    MAPS</a>
            </div>

        </div>



    </div>




</div>





{{-- <div class="project third_container full_width_banner max_height_banner">


    <div class="inner_container" style="justify-content: space-around!important;">

        <div>
            <img src="{{asset('assets/elias_bco.png')}}" alt="" class="logo">
</div>
<div class="text"> UN DESARROLLO DE:</div>
<div>
    <div class="w-100">
        <img src="{{asset('assets/forms_bco.png')}}" alt="" class="forms_logo">
    </div>
    <div class="w-100">
        {{-- <button class="white_button">SEMBLANZA</button> --}}
    </div>

</div>

</div>

</div>

<div class="form_container container">
    <form action="{{route('saveFormInput')}}" method="POST" id="homeForm">
        @csrf
        <div class="row">
            @php
            $name= $inputFields->filter(fn($s)=>$s->type=='name')->first();
            $mail= $inputFields->filter(fn($s)=>$s->type=='mail')->first();
            $phone= $inputFields->filter(fn($s)=>$s->type=='phone')->first();
            $msg= $inputFields->filter(fn($s)=>$s->type=='msg')->first();
            @endphp



            <div class="form_text">
                <div class="col-12">
                    Déjanos tus datos y nos pondremos en contacto contigo.
                </div>

            </div>


            @if($name)
            <div class="col-12">
                <div class="input_container">
                    <div class="label">
                        <label for="">{{$name->name}} @if($name->required) <span
                                class="req_input">*requerido</span>@endif</label>
                    </div>
                    <div class="input">
                        <input type="text" placeholder="{{$name->placeholder}}" @if($name->required)
                        required @endif name="input_name">
                    </div>
                </div>
            </div>
            @endif

            @if($mail)
            <div class="col-sm-12  @if($mail && $phone) col-md-6 @endif">
                <div class="input_container">
                    <div class="label">
                        <label for="">{{$mail->name}} @if($mail->required) <span
                                class="req_input">*requerido</span>@endif</label>
                    </div>
                    <div class="input">
                        <input type="email" placeholder="{{$mail->placeholder}}" @if($mail->required)
                        required @endif name="input_mail">
                    </div>
                </div>
            </div>
            @endif

            @if($phone)
            <div class="col-sm-12  @if($mail && $phone) col-md-6 @endif">
                <div class="input_container">
                    <div class="label">
                        <label for="">{{$phone->name}} @if($phone->required) <span
                                class="req_input">*requerido</span>@endif</label>
                    </div>
                    <div class="input">
                        <input type="text" placeholder="{{$phone->placeholder}}" @if($phone->required)
                        required @endif name="input_phone">
                    </div>
                </div>
            </div>
            @endif
            @if($msg)
            <div class="col-sm-12">
                <div class="input_container">
                    <div class="label">
                        <label for="">{{$msg->name}} @if($msg->required) <span
                                class="req_input">*requerido</span>@endif</label>
                    </div>
                    <div class="input">
                        <textarea name="input_message" placeholder="{{$msg->placeholder}}"
                            @if($msg->required) required @endif required></textarea>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="form_button_container">
            <button class="form_button black_button saveInputButtonn">ENVIAR</button>
        </div>

    </form>





</div>
<br>

<br>
<hr>
<br>


<div class="container_fluid final_container">
    <div class="row no_margin">
        <div class="col-6 col-md-5">
            <div class="img_cont left_image">

                <div class="text">
                    DISEÑADO POR
                </div>
                <img src="{{asset('assets/elias_gold.svg')}}" alt="img" class="bottom_img fright">

            </div>

        </div>
        <div class="d-none d-md-block col-2">
            <div class="divider" style="margin: 0 auto;"></div>
        </div>

        <div class="col-6 col-md-5">
            <div class="img_cont right_image">
                <div class="text">
                    DESARROLLADO POR
                </div>
                <img src="{{asset('assets/forms_gold.svg')}}" alt="img" class="bottom_img fleft">

            </div>
        </div>


        <div class="fourth_container button_container w-100 no_sm">
            <a class="btn primary_button black_button rounded center_all white" href="{{route('ubicacion')}}">CÓMO
                LLEGAR</a>
            <a class="btn primary_button black_button rounded center_all white"
                href="{{route('ubicacion')}}">SHOWROOM</a>
        </div>
    </div>



</div>




<div class="modal" tabindex="-1" role="dialog" id="videoModal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <img src="{{asset('assets/log.png')}}" alt="">
                </h5>
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="yt_video_container"></div>
            </div>

        </div>
    </div>
</div>

@endsection


@section('page_resources')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="//scripts.iconnode.com/82369.js"></script>
<script>
    $(document).ready(function() {
        @if(isset($video)&&$video->content!='')
        getYoutube();
        @endif


  });



  $('.close-modal').click(function(e) {
    e.preventDefault();
    $('.yt_video_container').children('iframe').attr('src', '');
  });


    function getYoutube(){
    const videoId = getId("{{isset($video)?$video->content:''}}");
    // const videoId = getId("https://www.youtube.com/watch?v=WZ2B5sn31qc");
let h = window.innerWidth<1045?315:500;

const iframeMarkup = '<iframe width="100%" height="'+h+'" src="//www.youtube.com/embed/' 
    + videoId + '" frameborder="0" allowfullscreen></iframe>';
    $('.yt_video_container').empty();
    $('.yt_video_container_home').empty();
    $('.yt_video_container').append(iframeMarkup);
    $('.yt_video_container_home').append(iframeMarkup);
    if( window.innerWidth<1045){
        // $('#videoModal').modal()
    }
   

}

function getId(url) {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);
    return (match && match[2].length === 11)
      ? match[2]
      : null;

}
    

function validateYoutubeLink(){
    let regYoutube= new RegExp(/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/);
    if(!regYoutube.test(($('#videoLink').val().toString()))){
        return alert('El link no es válido');
    }
    $('#saveYtLinkForm').submit()
    return true
}


$('.saveInputButtonn').click(function(e){

    let valid = true;
  $('[required]:visible').each(function() {
    if ($(this).is(':invalid') || !$(this).val()) valid = false;
  })
  if(valid){
    console.log('valiiid')
    showLoader();
  }
 
})




$('.home_slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  infinite:true,
  autoplay: true,
  autoplaySpeed: 2000,
});
$('.home_slider_sm').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  infinite:true,
  autoplay: true,
  autoplaySpeed: 2000,
});

</script>
@endsection