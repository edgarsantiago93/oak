@extends('mainlayout')

@section('page_content')




<style>
    .project_offset {
        background-image: url("{{asset('assets/project/plano.png')}}") !important;
    }

    .button_container>img {
        width: 100%;
        max-width: 200px;
    }

    @media screen and (max-width:768px) {
        .img_container {
            display: flex !important;
            justify-content: center !important;
            align-items: center !important;
        }
    }
</style>




<div class="offset_container adjust_height_photo">
    <div class="dark_section">
        <div class="container">
            <div class="title">
                PROYECTO
            </div>
        </div>
    </div>

    <div class="container">
        <div class="virtual offset_media_container map_photo  white_bg">
            <img src=" {{asset('assets/project/torrevista.jpg')}}" alt="">
        </div>



        <div class="button_container img_container" style="justify-content: space-around">
            <img src="{{asset('assets/elias_gold.svg')}}" alt="" class=" logo">
            <img src="{{asset('assets/forms_gold.svg')}}" alt="" class=" logo">

        </div>




        <div class="text_container">
            <div class="text no_b">

                <b> Respaldo y solidez , sinónimo de confianza </b>

                <br><br>
                Forms está conformado por un grupo de empresarios especializados en el ramo inmobiliario, mismos que han
                aportado solidez y seguridad a cada uno de sus proyectos, mientras ofrecen a socios inversionistas,
                empresas asociadas y entidades financieras, un valor incomparable de respaldo.
                <br><br>

                Forms tiene además, el compromiso de crear espacios únicos, contemplando sus atributos en un total
                urbano, lo que le suma funcionalidad y calidez a cada uno de sus desarrollos
                <br><br>

                <b>Despacho de arquitectura trayectoria emotiva y tangible</b>
                <br><br>

                ELIAS Estudio es una firma de arquitectos dedicados desde 1975 a la realización de proyectos
                arquitectónicos y a la promoción de desarrollos inmobiliarios de diversos géneros. Liderado por el
                arquitecto Roberto Elias Pessah, Elias Estudio se ha caracterizado por su calidad y si innovación en la
                creación de espacios y conceptos de alto valor agregado, que a su vez han creado marcas que se han
                posicionado como líderes en su especialidad.

            </div>

        </div>


    </div>
</div>








@endsection