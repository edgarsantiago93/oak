@extends('mainlayout',['showButtons'=>false])


@section('page_content')

<style>
    .ytIframe {
        width: 100%;
    }

    .yt_video_container {
        width: 100%;
        height: 100%;
        min-height: 500px;
    }
</style>

<div class="offset_container mb0">

    <div class="light_section">
        <div class="container">
            <div class="title">
                MEDIA
            </div>
        </div>
    </div>

    <div class="container mb-5">
        <div class="contact media_container_no_offset">
            <div class="yt_video_container">

            </div>
        </div>
    </div>


    <div class="container mt-4">
        <div class="media_container pt-5">
            @foreach ($mediaPosts as $mp)
            <div class="media_instance">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="media_img">
                            <a href="{{asset('images/'.$mp->imgLink())}}" target="_blank"> <img
                                    src="{{asset('images/'.$mp->imgLink())}}" alt="image"></a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="h-100 center_all">
                            <div class="media_content_wrapper">
                                <div class="media_post_title">{{$mp->title}}</div>
                                <div class="media_post_content">
                                    {!!$mp->content!!}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>




    <div class="blog_dark_container">

        <div class="row no_padding no_margin">
            <div class="col-sm-12 col-md-5">
                <div class="blog_section_title">
                    BLOG
                </div>
            </div>
            <div class="col-sm-12 col-md-7">
                <div class="blog_section_text">
                    En este espacio encontrarás información y consejos para comenzar a vivir
                    una <b> experiencia icónica.</b>
                </div>
            </div>
        </div>




        <div class="row no_padding no_margin">


            <div class="col-sm-12 col-md-8 offset-md-4">
                <div class="container mt-4">
                    <div class="media_container pt-5">
                        @foreach ($blogPosts as $mp)
                        <div class="media_instance">

                            <div class="row no_padding">

                                <div class="col-sm-12 col-md-4">
                                    <div class="media_img">
                                        <a href="{{asset('images/'.$mp->imgLink())}}" target="_blank"> <img
                                                src="{{asset('images/'.$mp->imgLink())}}" alt="image"></a>
                                    </div>
                                </div>


                                <div class="col-sm-12 col-md-8">
                                    <div class="h-100 center_all">
                                        <div class="media_content_wrapper">
                                            <div class="media_post_title">{{$mp->title}}</div>
                                            <div class="media_post_content blog_content">


                                                {!!$mp->content!!}
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>


    </div>

</div>


@endsection

@section('page_resources')

<script>
    $(document).ready(function() {
        @if(isset($video)&&$video->content!='')
        getYoutube();
        @endif
  });



  $('.close-modal').click(function(e) {
    e.preventDefault();
    $('.yt_video_container').children('iframe').attr('src', '');
  });


    function getYoutube(){
    const videoId = getId("{{isset($video)?$video->content:''}}");
    // const videoId = getId("https://www.youtube.com/watch?v=WZ2B5sn31qc");
const iframeMarkup = '<iframe width="100%" height="500" src="//www.youtube.com/embed/' 
    + videoId + '" frameborder="0" allowfullscreen class=ytIframe""></iframe>';
    $('.yt_video_container').empty();
    $('.yt_video_container').append(iframeMarkup);
    $('#videoModal').modal()

}

function getId(url) {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);
    return (match && match[2].length === 11)
      ? match[2]
      : null;

}
    

function validateYoutubeLink(){
    let regYoutube= new RegExp(/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/);
    if(!regYoutube.test(($('#videoLink').val().toString()))){
        return alert('El link no es válido');
    }
showLoader();
    $('#saveYtLinkForm').submit()
    return true
}

</script>
@endsection