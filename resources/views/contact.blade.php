@extends('mainlayout',['showButtons'=>false,'bigFooter'=>true])


@section('page_content')



<div class="offset_container">

    <div class="light_section">
        <div class="container">
            <div class="title">
                CONTACTO
            </div>
        </div>
    </div>

    <div class="container">
        {{-- <div class="contact media_container_no_offset"> --}}
        <div class="contact ">
            <div class="form_container container">

                <form action="{{route('saveFormInput')}}" method="POST">
                    @csrf
                    <div class="row">

                        @php
                        $name= $inputFields->filter(fn($s)=>$s->type=='name')->first();
                        $mail= $inputFields->filter(fn($s)=>$s->type=='mail')->first();
                        $phone= $inputFields->filter(fn($s)=>$s->type=='phone')->first();
                        $msg= $inputFields->filter(fn($s)=>$s->type=='msg')->first();
                        @endphp

                        <div class="form_text">
                            <div class="col-12">
                                Déjanos tus datos y nos pondremos en contacto contigo.
                            </div>

                        </div>

                        @if($name)
                        <div class="col-12">
                            <div class="input_container">
                                <div class="label">
                                    <label for="">{{$name->name}}</label>
                                </div>
                                <div class="input">
                                    <input type="text" placeholder="{{$name->placeholder}}" @if($name->required)
                                    required @endif name="input_name">
                                </div>
                            </div>
                        </div>
                        @endif

                        @if($mail)
                        <div class="col-sm-12  @if($mail && $phone) col-md-6 @endif">
                            <div class="input_container">
                                <div class="label">
                                    <label for="">{{$mail->name}}</label>
                                </div>
                                <div class="input">
                                    <input type="email" placeholder="{{$mail->placeholder}}" @if($mail->required)
                                    required @endif name="input_mail">
                                </div>
                            </div>
                        </div>
                        @endif

                        @if($phone)
                        <div class="col-sm-12  @if($mail && $phone) col-md-6 @endif">
                            <div class="input_container">
                                <div class="label">
                                    <label for="">{{$phone->name}}</label>
                                </div>
                                <div class="input">
                                    <input type="text" placeholder="{{$phone->placeholder}}" @if($phone->required)
                                    required @endif name="input_phone">
                                </div>
                            </div>
                        </div>
                        @endif
                        @if($msg)
                        <div class="col-sm-12">
                            <div class="input_container">
                                <div class="label">
                                    <label for="">{{$msg->name}}</label>
                                </div>
                                <div class="input">
                                    <textarea name="input_message" placeholder="{{$msg->placeholder}}"
                                        @if($msg->required) required @endif></textarea>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="form_button_container">
                        <button class="form_button black_button saveInputButtonn">ENVIAR</button>
                    </div>

                </form>




            </div>
        </div>

        {{-- <div class="map_button_container">
            <button class="btn blue_button mid_width">....</button>
        </div> --}}
    </div>
</div>



@endsection

@section('page_resources')
<script>
    $('.saveInputButtonn').click(function(e){
    let valid = true;
  $('[required]:visible').each(function() {
    if ($(this).is(':invalid') || !$(this).val()) valid = false;
  })
  if(valid){
    console.log('valiiid')
    showLoader();
  }
})


</script>
@endsection