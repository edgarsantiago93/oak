@extends('mainlayout',['showButtons'=>false,'bigFooter'=>true])


@section('page_content')



<div class="offset_container adjust_height">

    <div class="dark_section">
        <div class="container">
            <div class="title">
                UBICACIÓN
            </div>
        </div>
    </div>

    <div class="container">

        <div class="virtual offset_media_container map_photo nobg">
            <a target="_blank" href="https://g.page/oak-58---high-living?share"><img
                    src="{{asset('assets/map/map_home.png')}}" alt=""></a>
        </div>

        <div class="map_button_container">
            {{-- <a target="_blank" class="btn blue_button mr center_all white" href="https://g.page/oak-58---high-living?share">Desarrollo</a> --}}
            {{-- <a target="_blank" class="btn blue_button ml center_all white" href="https://goo.gl/maps/nvppNYM11GMcwbLd7">Showroom</a> --}}

            <a target="_blank" class="btn primary_button center_all"
                href="https://g.page/oak-58---high-living?share">Desarrollo</a>
            <a target="_blank" class="btn primary_button center_all"
                href="https://goo.gl/maps/nvppNYM11GMcwbLd7">Showroom</a>

        </div>



        <div class="col-12" style="  margin-bottom: 200px;    text-align: center;
        font-size: 24px;
        letter-spacing: 0px;
        font-weight:300;">
            Ubicación con excelente conectividad a establecimientos de
            primera necesidad, negocios y entretenimiento.
            Con dos salidas de acceso; Avenida Atlixcáyotl y Angelópolis, la zona
            de mayor categoría y plusvalía en los últimos años.
        </div> <br>
        <div class="col-12" style="  margin-bottom: 200px;    text-align: center;
        font-size: 26px;
        letter-spacing: 0px;
        font-weight:400;">
            <div class="address_text">
                Desarrollo: Vía Atlixcayotl #6522, Puebla, C.P. 72828
            </div>
        </div>
    </div>
</div>


@endsection