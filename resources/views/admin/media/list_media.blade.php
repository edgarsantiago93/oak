@extends('admin.mainLayout')



@section('page_content')



<div class="content_container">
    <div class="page_title">
        Lista de posts de media
    </div>




    <div class="container">
        <div class="top_video_container">

            <div class="row no_margin video_row">
                <div class="col-sm-12 col-md-6">
                    <div class="yt_video_container _center">
                        No hay video
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <form action="{{route('saveSettings')}}" method="POST" id="saveYtLinkForm" class="create_form">
                        @csrf

                        <div class="video_settings_containedr">
                            <div class="input_container">
                                <input type="text" name="media_video" placeholder="Link de video de Youtube"
                                    class="input_element" style=" height: 50px;" id="videoLink"
                                    value="{{isset($video)?$video->content:''}}">
                            </div>

                            <div class="media_video_button_container">
                                <button class="btn btn-success validateYtLink" type="button">
                                    Guardar Video
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <br>
    <hr><br>



    <div class="crud_row">
        <button class="btn btn-primary" onclick="window.location.href='{{route('admin.create_media')}}'">
            Nuevo post de media
        </button>
    </div>


    <div class="form_container_">




        <table>
            <thead>
                <th>Título</th>
                <th>Fecha</th>
                <th>Status</th>
                <th></th>
            </thead>


            @foreach ($media as $p)
            <tr>
                <td>{{$p->title}}</td>
                <td>{{$p->getDate()}}</td>
                <td>{{$p->active?'Activo':'Inactivo'}}</td>

                <td><button class="btn btn-outline-info"
                        onclick="window.location.href='{{route('admin.read_media',['uuid'=>$p->uuid])}}'">Ver</button>
                </td>
            </tr>

            @endforeach

        </table>


    </div>

    {{-- <div class="input_container">
    <input type="text" name="user_name" placeholder="Nombre"
       class="input_element  @error('user_name') is-invalid @enderror" required
       value="{{$user->name}}">
    @error('user_name')
    <div class="error_container alert-danger">{{ $message }}</div>
    @enderror
</div> --}}





</div>






@endsection






@section('page_resources')



<script>
    $(document).ready(function(){
        getYoutube()
});


$('.validateYtLink').click(function(){
validateYoutubeLink();
});



function getYoutube(){
    const videoId = getId("{{isset($video)?$video->content:''}}");
const iframeMarkup = '<iframe width="560" height="315" src="//www.youtube.com/embed/' 
    + videoId + '" frameborder="0" allowfullscreen></iframe>';

    $('.yt_video_container').empty();
    $('.yt_video_container').append(iframeMarkup);
}

function getId(url) {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);
    return (match && match[2].length === 11)
      ? match[2]
      : null;

}
    

function validateYoutubeLink(){
    let regYoutube= new RegExp(/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/);
    if(!regYoutube.test(($('#videoLink').val().toString()))){
        return alert('El link no es válido');
    }
showLoader();
    $('#saveYtLinkForm').submit()
    return true
}



</script>
@endsection