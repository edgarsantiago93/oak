@extends('admin.mainLayout')



@section('page_content')



<div class="content_container">
    <div class="page_title">
        Lista de posts
    </div>

    <div class="crud_row">
        <button class="btn btn-primary" onclick="window.location.href='{{route('admin.create_post')}}'">
            Nuevo
            post
        </button>
    </div>



    <div class="form_container_">




        <table>
            <thead>
                <th>Título</th>
                <th>Fecha</th>
                <th>Status</th>
                <th></th>
            </thead>


            @foreach ($posts as $p)
            <tr>
                <td>{{$p->title}}</td>
                <td>{{$p->getDate()}}</td>
                <td>{{$p->active?'Activo':'Inactivo'}}</td>

                <td><button class="btn btn-outline-info"
                        onclick="window.location.href='{{route('admin.read_post',['uuid'=>$p->uuid])}}'">Ver</button>
                </td>
            </tr>

            @endforeach

        </table>


    </div>

    {{-- <div class="input_container">
    <input type="text" name="user_name" placeholder="Nombre"
       class="input_element  @error('user_name') is-invalid @enderror" required
       value="{{$user->name}}">
    @error('user_name')
    <div class="error_container alert-danger">{{ $message }}</div>
    @enderror
</div> --}}





</div>






@endsection






@section('page_resources')



<script>
    $(document).ready(function(){
      

});
</script>
@endsection