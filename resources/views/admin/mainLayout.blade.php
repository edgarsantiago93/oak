<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>OAK | ADMIN</title>

    <!-- Fonts -->
    {{-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous"> --}}
    <!-- Styles -->

    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('/css/admin.css')}}">
    <style>
    </style>
</head>

<body>


    <div class="loader_wrapper">
        <div class="loader_container _center">
            <div>
                Cargando...
            </div>
        </div>
    </div>



    <div class="container-fluid no_padding main_admin_container">

        <div class="left_wrapper">
            <div class="left_menu_container">

                <div class="logo_container">
                    <img src="{{asset('tempAssets/logo.png')}}" alt="">
                </div>


                <table class="menu_table">
                    <tr>
                        <td>
                            <div class="menu_item">
                                <a href="{{route('admin.forms')}}">Formulario</a>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div class="menu_item">
                                <a href="{{route('admin.banners')}}">Banners</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="menu_item">
                                <a href="{{route('admin.blog')}}">Blog</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="menu_item">
                                <a href="{{route('admin.media')}}">Media</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="menu_item">
                                <a href="{{route('admin.updates')}}">Avance de obra</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="menu_item">
                                <a href="{{route('admin.settings')}}">Ajustes</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="menu_item">
                                <a href="{{route('admin.messages')}}">Mensajes</a>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>


        <div class="right_wrapper">
            <div class="top_user_menu">
                <div class="user_button">
                    <button class="grey_button btn modal_logout">
                        Hola, {{Auth::user()->name}}
                    </button>

                </div>
            </div>
            <div class="right_container">
                @yield('page_content')
            </div>
        </div>


        <div class="modal" tabindex="-1" role="dialog" id="confirmModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal">Confirmar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p id="modal_msg"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="confirmModalButton">Continuar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>


</body>




{{-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
    integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous">
</script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script> --}}

<script src="{{asset('/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('/js/axios.min.js')}}"></script>
<script src="{{asset('/js/bootstrap.min.js')}}"></script>

@yield('page_resources')
<script>
    axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN' : '{{csrf_token()}}'
};



    function confirmModal(txt,callback){
$('#modal_msg').text(txt);
$('#confirmModal').modal();
if(callback){
    $('#confirmModalButton').off()
    $('#confirmModalButton').click(function(){
        callback();
    })

}

}

    function showLoader(){
        $('.loader_wrapper').fadeIn('fast');
    }
    function hideLoader(){
        $('.loader_wrapper').hide();
    }


    
    $('.modal_logout').click(function(){
        confirmModal('¿Deseas cerrar sesión?',function(){
            window.location.href='{{url('/logout')}}';
        });
    })
</script>

</html>