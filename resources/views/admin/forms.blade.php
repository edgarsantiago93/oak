@extends('admin.mainLayout')



@section('page_content')



<div class="content_container">
    {{-- <div class="crud_row">
        <button class="btn btn-primary">Nuevo formulario</button>
    </div> --}}

    <div class="page_title">
        Campos activos
    </div>

    {{-- 
    <div class="container">
        <div class="top_video_container">

            <div class="row no_margin video_row">

                <div class="col-sm-12">
                    <form action="{{route('saveSettings')}}" method="POST" id="saveYtLinkForm" class="create_form">
    @csrf

    <div class="video_settings_containedr">
        <div class="input_container">
            <input type="email" name="form_mail" placeholder="Correo al que se enviara los mensajes de contacto"
                class="input_element" style=" height: 50px;" id="videoLink"
                value="{{isset($mailToSend)?$mailToSend->content:''}}" required>
        </div>

        <div class="media_video_button_container">
            <button class="btn btn-success validateMailLink" type="button">
                Guardar Correo
            </button>
        </div>
    </div>
    </form>

</div>
</div>
</div>
</div> --}}
<br>
<hr><br>




<div class="container">
    <div class="form_container">


        <form action="{{route('saveForm')}}" method="POST">
            @csrf
            <input type="text" name="uuid" hidden>
            <table>
                <thead>
                    <th></th>
                    <th>Placeholder</th>
                    <th>Requerido</th>
                    <th>Mostrar</th>
                </thead>

                @php
                $name= $inputs->filter(fn($s)=>$s->type=='name')->first();
                $mail= $inputs->filter(fn($s)=>$s->type=='mail')->first();
                $phone= $inputs->filter(fn($s)=>$s->type=='phone')->first();
                $msg= $inputs->filter(fn($s)=>$s->type=='msg')->first();
                @endphp

                <tr>
                    <td class="label_name">Nombre</td>
                    <td><input type="text" name="name_placeholder"
                            value="@if(isset($name)){{$name->placeholder}}@endif"></td>
                    <td class="check_input"><input type="checkbox" name="name_required"
                            @if(isset($name)&&$name->required) checked @endif></td>
                    <td class="check_input"><input type="checkbox" name="name_active" @if(isset($name)&&$name->active)
                        checked @endif></td>
                </tr>

                <tr>
                    <td class="label_name">Correo</td>
                    <td><input type="text" name="mail_placeholder"
                            value="@if(isset($mail)){{$mail->placeholder}}@endif"></td>
                    <td class="check_input"><input type="checkbox" name="mail_required"
                            @if(isset($mail)&&$mail->required) checked @endif></td>
                    <td class="check_input"><input type="checkbox" name="mail_active" @if(isset($mail)&&$mail->active)
                        checked @endif></td>
                </tr>

                <tr>
                    <td class="label_name">Teléfono</td>
                    <td><input type="text" name="phone_placeholder"
                            value="@if(isset($phone)){{$phone->placeholder}}@endif">
                    </td>
                    <td class="check_input"><input type="checkbox" name="phone_required"
                            @if(isset($phone)&&$phone->required)
                        checked @endif></td>
                    <td class="check_input"><input type="checkbox" name="phone_active"
                            @if(isset($phone)&&$phone->active)
                        checked @endif></td>
                </tr>

                <tr>
                    <td class="label_name">Mensaje</td>
                    <td><input type="text" name="msg_placeholder" value="@if(isset($msg)){{$msg->placeholder}}@endif">
                    </td>
                    <td class="check_input"><input type="checkbox" name="msg_required" @if(isset($msg)&&$msg->required)
                        checked @endif></td>
                    <td class="check_input"><input type="checkbox" name="msg_active" @if(isset($msg)&&$msg->active)
                        checked @endif></td>
                </tr>

            </table>
            <div class="col-12 t-center">
                <button class="btn btn-success saveButton" type="submit">Guardar</button>
            </div>

        </form>

    </div>
</div>





</div>






@endsection






@section('page_resources')



<script>
    $('.saveButton').click(function(){
    showLoader();
});  


$('.validateMailLink').click(function(){
validateYoutubeLink();
});

function validateYoutubeLink(){
let valid =true;

    if(!valid){
        return alert('El link no es válido');
    }
showLoader();
    $('#saveYtLinkForm').submit()
    return true
}

</script>
@endsection