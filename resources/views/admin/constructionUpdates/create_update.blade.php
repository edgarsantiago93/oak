@extends('admin.mainLayout')



@section('page_content')



<div class="content_container">


    <div class="page_title">
        Nuevo post de avance
    </div>


    <div class="form_container" style="margin-top: 30px;">

        <form action="{{route('saveUpdate')}}" method="POST" id="saveFormForm" class="create_form">
            @csrf
            <div class="input_container">
                <input type="text" name="post_title" placeholder="Fecha (título)" class="input_element"
                    style="    height: 50px; width:100%!important" id="postTitle">
                @error('post_title')
                <div class="error_container alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <input type="text" name="post_content" id="postMessage" hidden>
            <input type="text" name="post_photo" id="postPhoto" hidden>
        </form>

        <textarea class="messageContent" id="messageContent"></textarea>

        <div id="photoContainer" class="dropzone"></div>
        <div class="error_container alert-danger" id="dzoneerror"></div>


        <div class="" style="text-align: center">
            <div class="button"> <button class="btn btn-success saveButton">Guardar</button></div>
        </div>

    </div>




</div>




@endsection




@section('page_resources')

<script src="https://cdn.tiny.cloud/1/nzrm0s4rg9qzi1zgjblva8j9m0159iz68mv8xw7zm3rmmgxb/tinymce/5/tinymce.min.js"
    referrerpolicy="origin"></script>

<link rel="stylesheet" href="{{asset('css/dropzone.css')}}">
<script src="{{asset('js/dropzone.js')}}"></script>
{{-- <script src="{{asset('js/tinymce.min.js')}}"></script> --}}

<script>
    Dropzone.autoDiscover = false;
Dropzone.prototype.defaultOptions.dictDefaultMessage = "Da click aquí o arrastra tu archivo";
Dropzone.prototype.defaultOptions.dictFallbackMessage = "Por favor utiliza Google Chrome";
Dropzone.prototype.defaultOptions.dictFileTooBig = "El archivo es muy grande, selecciona uno de máximo 10mb";
Dropzone.prototype.defaultOptions.dictInvalidFileType = "No puedes subir archivos de este tipo.";
Dropzone.prototype.defaultOptions.dictResponseError = "Ocurrió un error, intenta de nuevo.";
Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancelar";
Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "¿Estas seguro?";
Dropzone.prototype.defaultOptions.dictRemoveFile = "Borrar archivo";
Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "Solo puedes subir 4 archivos.";

 

$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
  initDropzone()
tinymce.init({
      selector: '.messageContent',
      plugins: ' autolink lists  hr anchor pagebreak',
      toolbar_mode: 'floating',
      block_unsupported_drop: true,
      automatic_uploads: false,
      toolbar:"undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent",

    });

});



let dz;
let photoArrray =[];
function initDropzone(){
dz= new Dropzone("#photoContainer",{ 
      url: "{{url('image/upload/store')}}",
      paramName: "file", // The name that will be used to transfer the file
      maxFilesize: 10, // MB
      maxFiles:1,
      acceptedFiles: ".jpeg,.jpg,.png,.gif",
      addRemoveLinks: true,
      timeout: 50000,
      sending: function(file, xhr, formData) {
    formData.append("_token", "{{ csrf_token() }}");
},
      renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
               return time+file.name;
            },
  
            removedfile: function(file) 
            {
                var name = file.upload.filename;
                $.ajax({
                    headers: {
                                'X-CSRF-TOKEN': "{{csrf_token()}}"
                            },
                    type: 'POST',
                    url: '{{ url("image/delete") }}',
                    data: {filename: name},
                    success: function (data){
                    },
                    error: function(e) {
                        console.log(e);
                    }});
                    var fileRef;
                    return (fileRef = file.previewElement) != null ? 
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function(file, response) 
            {
                console.log(response);

                photoArrray.push(response.imUuid);
            },
            error: function(file, response)
            {
               $('#dzoneerror').show()
               $('#dzoneerror').empty()
               $('#dzoneerror').text(response)
               setTimeout(function(){
                  $('#dzoneerror').fadeOut('fast')
               },2000)
            this.removeFile(file);
            }      
    });
}



$('.saveButton').click(function(){
submitForm();
});

function submitForm(){
    let title =$('#postTitle');
    let message =$('#postMessage');
    let photo =$('#postPhoto');
    let messageContent = tinymce.get("messageContent").getContent();
    message.val(messageContent);

   if(dz.files.filter(function(it){return it.accepted}).length>0){

    if(title.val().length<3 || message.val().length<3){
        $('#dzoneerror').empty()
        $('#dzoneerror').show()
               $('#dzoneerror').text('Por favor agrega un título y un mensaje para el post')
               setTimeout(function(){
                  $('#dzoneerror').fadeOut('fast')
               },2000)
    }else{
        photo.val(photoArrray);
        showLoader();
      $('#saveFormForm').submit();
    }
      
   }
   else{
      $('#dzoneerror').empty()
      $('#dzoneerror').show()
               $('#dzoneerror').empty()
               $('#dzoneerror').text('Por favor agrega por lo menos 1 foto al posteo.')
               setTimeout(function(){
                  $('#dzoneerror').fadeOut('fast')
               },2000)
   }
 
}




</script>
@endsection