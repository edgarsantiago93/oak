@extends('admin.mainLayout')



@section('page_content')

<style>
    table {
        width: 100%;
    }
</style>
<div class="content_container">
    {{-- <div class="crud_row">
        <button class="btn btn-primary">Nuevo formulario</button>
    </div> --}}

    <div class="page_title">
        Ajustes
    </div>

    <div class="form_container">
        {{-- 
        <table>
            <thead>
                <th>Nombre</th>
                <th>Ubicación</th>
            </thead>
        </table> --}}



        <form action="{{route('saveSettings')}}" method="POST">
            @csrf
            <input type="text" name="uuid" hidden>
            <table class="save_settings_form">
                <thead>
                    <th></th>
                    <th>Link</th>
                    <th>Visible</th>
                </thead>

                @php
                $yt= $settings->filter(fn($s)=>$s->type=='youtube')->first();
                $wa= $settings->filter(fn($s)=>$s->type=='whatsapp')->first();
                $ins= $settings->filter(fn($s)=>$s->type=='instagram')->first();
                $fb= $settings->filter(fn($s)=>$s->type=='facebook')->first();
                $li= $settings->filter(fn($s)=>$s->type=='linkedin')->first();
                $sp= $settings->filter(fn($s)=>$s->type=='spotify')->first();
                $tr= $settings->filter(fn($s)=>$s->type=='track_code')->first();
                $trbody= $settings->filter(fn($s)=>$s->type=='track_code_body')->first();
                $iv= $settings->filter(fn($s)=>$s->type=='initial_video')->first();
                $ph= $settings->filter(fn($s)=>$s->type=='phone')->first();

                $pdf= $settings->filter(fn($s)=>$s->type=='pdf_file')->first();
                $book= $settings->filter(fn($s)=>$s->type=='book_file')->first();
                @endphp

                <tr>
                    <td colspan="2" class="two_span">
                        <div>
                            Redes Sociales
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="label_name">Youtube</td>
                    <td><input type="text" placeholder="Link" name="youtube_link"
                            value="@if(isset($yt)){{$yt->content}}@endif"></td>
                    <td class="check_input"><input type="checkbox" name="youtube_visible" @if(isset($yt)&&$yt->active)
                        checked @endif></td>
                </tr>
                <tr>
                    <td class="label_name">WhatsApp (Agregar "+52")</td>
                    <td><input type="text" placeholder="Link" name="whatsapp_link"
                            value="@if(isset($wa)){{$wa->content}}@endif"></td>
                    <td class="check_input"><input type="checkbox" name="whatsapp_visible"
                            @if(isset($wa)&&$wa->active)checked @endif></td>
                </tr>

                <tr class="label_name">
                    <td>Instagram</td>
                    <td><input type="text" placeholder="Link" name="instagram_link"
                            value="@if(isset($ins)){{$ins->content}}@endif"></td>
                    <td class="check_input"> <input type="checkbox" name="instagram_visible"
                            @if(isset($ins)&&$ins->active)checked @endif></td>
                </tr>
                <tr class="label_name">
                    <td>Facebook</td>
                    <td><input type="text" placeholder="Link" name="facebook_link"
                            value="@if(isset($fb)){{$fb->content}}@endif"></td>
                    <td class="check_input"><input type="checkbox" name="facebook_visible"
                            @if(isset($fb)&&$fb->active)checked @endif></td>
                </tr>
                <tr class="label_name">
                    <td>LinkedIn</td>
                    <td><input type="text" placeholder="Link" name="linkedin_link"
                            value="@if(isset($li)){{$li->content}}@endif"></td>
                    <td class="check_input"> <input type="checkbox" name="linkedin_visible"
                            @if(isset($li)&&$li->active)checked @endif></td>
                </tr>

                <tr class="label_name">
                    <td>Spotify</td>
                    <td>
                        <textarea type="text" placeholder="Link"
                            name="spotify_link">@if(isset($sp)){{$sp->content}}@endif</textarea>
                    </td>
                    <td class="check_input"><input type="checkbox" name="spotify_visible"
                            @if(isset($sp)&&$sp->active)checked @endif></td>
                </tr>

                <tr class="label_name">
                    <td>Teléfono</td>
                    <td><input type="text" placeholder="Teléfono" name="phone_link"
                            value="@if(isset($ph)){{$ph->content}}@endif"></td>

                    <td class="check_input"><input type="checkbox" name="phone_visible"
                            @if(isset($ph)&&$ph->active)checked @endif></td>
                </tr>


                <tr>
                    <td colspan="2" class="two_span">
                        <div>
                            Códigos Embed y Analytics
                        </div>
                    </td>
                </tr>




                <tr class="label_name">
                    <td>Código Google Anaytics (Head)</td>
                    <td>
                        <textarea type="text" placeholder="Código"
                            name="track_link">@if(isset($tr)){{$tr->content}}@endif</textarea>

                    </td>
                    <td class="check_input"><input type="checkbox" name="track_visible"
                            @if(isset($tr)&&$tr->active)checked @endif></td>
                </tr>


                <tr class="label_name">
                    <td>Código Google Anaytics (Body)</td>
                    <td>
                        <textarea type="text" placeholder="Código"
                            name="track_body_link">@if(isset($trbody)){{$trbody->content}}@endif</textarea>
                    </td>
                    <td class="check_input"><input type="checkbox" name="track_body_visible"
                            @if(isset($trbody)&&$trbody->active)checked @endif></td>
                </tr>


                <tr>
                    <td colspan="2" class="two_span">
                        <div>
                            Sección Home
                        </div>

                    </td>
                </tr>
                <tr class="label_name">
                    <td>Video Institucional</td>
                    <td><input type="text" placeholder="Link video de youtube" name="initial_video"
                            value="@if(isset($iv)){{$iv->content}}@endif"></td>
                    <td class="check_input"> <input type="checkbox" name="initial_video_visible"
                            @if(isset($iv)&&$iv->active)checked @endif></td>
                </tr>

                <tr>
                    <td colspan="2" class="two_span">
                        <div>
                            Archivos
                        </div>

                    </td>
                </tr>
                <tr class="label_name">
                    <td>PDF</td>


                    @if(!isset($pdf) || $pdf->content =='')
                    <td colspan="2">
                        <input type="text" name="pdf_file" value="@if(isset($pdf)){{$pdf->content}}@endif" hidden
                            id="pdfInput">
                        <div id="pdfFileContainer" class="dropzone dz_container"></div>
                        <div class="error_container alert-danger pdf_error_zone" id="dzoneerror"></div>
                    </td>
                    @else
                    <td>
                        <input type="text" name="pdf_file" value="@if(isset($pdf)){{$pdf->content}}@endif" hidden
                            id="pdfInput">
                        <a class="file_link btn btn-info white" target="_blank"
                            href="{{asset('images/'.$pdf->getFileLink())}}">Ver
                            archivo</a></td>
                    <td>

                        <button class="btn btn-danger deleteFileButton" data-type='pdf_file'
                            data-fileid="{{$pdf->getFileId()}}" type="button">Borrar
                            archivo</button>

                    </td>
                    @endif
                </tr>


                <tr class="label_name">
                    <td>Book</td>


                    @if(!isset($book) || $book->content =='')
                    <td colspan="2">
                        <input type="text" name="book_file" value="@if(isset($book)){{$book->content}}@endif" hidden
                            id="bookInput">
                        <div id="bookFileContainer" class="dropzone dz_container"></div>
                        <div class="error_container alert-danger book_error_zone" id="dzoneerror_"></div>
                    </td>
                    @else
                    <td>
                        <input type="text" name="book_file" value="@if(isset($book)){{$book->content}}@endif" hidden
                            id="bookInput">
                        <a class="file_link btn-info btn white" target="_blank"
                            href="{{asset('images/'.$book->getFileLink())}}">Ver
                            archivo</a></td>
                    <td>

                        <button class="btn btn-danger deleteFileButton" data-type='book_file'
                            data-fileid="{{$book->getFileId()}}" type="button">Borrar
                            archivo</button>
                    </td>
                    @endif
                </tr>




            </table>
            <div class="col-12 t-center">
                <button class="btn btn-success saveButton" type="submit">Guardar</button>
            </div>
        </form>
    </div>
</div>

<form action="{{route('deleteImage')}}" method="POST" id="deleteFileIdForm">
    @csrf
    <input type="text" hidden value="" name="img_id" id="deleteFileIdInput">
    <input type="text" hidden value="" name="deletion_type" id="deletionTypeInput">
</form>




@endsection




@section('page_resources')

<link rel="stylesheet" href="{{asset('css/dropzone.css')}}">
<script src="{{asset('js/dropzone.js')}}"></script>
<script>
    $('.saveButton').click(function(){
    showLoader()
})





Dropzone.autoDiscover = false;
Dropzone.prototype.defaultOptions.dictDefaultMessage = "Da click aquí o arrastra tu archivo";
Dropzone.prototype.defaultOptions.dictFallbackMessage = "Por favor utiliza Google Chrome";
Dropzone.prototype.defaultOptions.dictFileTooBig = "El archivo es muy grande, selecciona uno de máximo 10mb";
Dropzone.prototype.defaultOptions.dictInvalidFileType = "No puedes subir archivos de este tipo.";
Dropzone.prototype.defaultOptions.dictResponseError = "Ocurrió un error, intenta de nuevo.";
Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancelar";
Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "¿Estas seguro?";
Dropzone.prototype.defaultOptions.dictRemoveFile = "Borrar archivo";
Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "Solo puedes subir 4 archivos.";

 

$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });



  @if(!isset($book) || $book->content =='')
  initDropzoneBook()
  @endif
  @if(!isset($pdf)|| $pdf->content =='')
  initDropzonePdf()
  @endif
  

});



let dz_pdf,dz_book;
let pdfArrray =[];
let bookArrray =[];



function initDropzoneBook(){

    dz_book= new Dropzone("#bookFileContainer",{ 
      url: "{{url('image/upload/store')}}",
      paramName: "file", // The name that will be used to transfer the file
      maxFilesize: 10, // MB
      maxFiles:1,
      acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf",
      addRemoveLinks: true,
      timeout: 50000,
      sending: function(file, xhr, formData) {
    formData.append("_token", "{{ csrf_token() }}");
},
      renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
               return 'file_'+time+file.name;
            },
            removedfile: function(file) 
            {
                var name = file.upload.filename;
                $.ajax({
                    headers: {
                                'X-CSRF-TOKEN': "{{csrf_token()}}"
                            },
                    type: 'POST',
                    url: '{{ url("image/delete") }}',
                    data: {filename: name},
                    success: function (data){
                    },
                    error: function(e) {
                        console.log(e);
                    }});
                    var fileRef;
                    return (fileRef = file.previewElement) != null ? 
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function(file, response) 
            {
                console.log(response)
                $('#bookInput').val(response.imUuid)
            },
            error: function(file, response)
            {
            dzError('.book_error_zone',response)
            this.removeFile(file);
            }      
    });

}
function initDropzonePdf(){
    dz_pdf= new Dropzone("#pdfFileContainer",{ 
      url: "{{url('image/upload/store')}}",
      paramName: "file", // The name that will be used to transfer the file
      maxFilesize: 10, // MB
      maxFiles:1,
      acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf",
      addRemoveLinks: true,
      timeout: 50000,
      sending: function(file, xhr, formData) {
    formData.append("_token", "{{ csrf_token() }}");
},
      renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
               return 'file_'+time+file.name;
            },
            removedfile: function(file) 
            {
                var name = file.upload.filename;
                $.ajax({
                    headers: {
                                'X-CSRF-TOKEN': "{{csrf_token()}}"
                            },
                    type: 'POST',
                    url: '{{ url("image/delete") }}',
                    data: {filename: name},
                    success: function (data){
                    },
                    error: function(e) {
                        console.log(e);
                    }});
                    var fileRef;
                    return (fileRef = file.previewElement) != null ? 
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function(file, response) 
            {
                console.log(response);
                pdfArrray.push(response.imUuid);
                $('#pdfInput').val(response.imUuid)

            },
            error: function(file, response)
            {
            dzError('.pdf_error_zone',response)
            this.removeFile(file);
            }      
    });

}




function dzError(zone, msg){
    $(zone).show()
               $(zone).empty()
               $(zone).text(response)
               setTimeout(function(){
                  $(zone).fadeOut('fast')
               },2000)
}


$('.deleteFileButton').click(function(){
    let id = $(this).data('fileid')
    let type = $(this).data('type')
    confirmModal('¿Deseas borrar el archivo?',function(){
        deleteFile(id,type)
    });
})



function deleteFile(id,type){
  
    showLoader(); 
      $('#deleteFileIdInput').val(id);
      $('#deletionTypeInput').val(type);
    $('#deleteFileIdForm').submit();               
}

</script>
@endsection