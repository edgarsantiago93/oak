@extends('admin.mainLayout')



@section('page_content')



<div class="content_container">


    <div class="page_title">
        Editar banner
    </div>


    <div class="form_container" style="margin-top: 30px;">

        <form action="{{route('editBanner')}}" method="POST" id="saveFormForm" class="create_form">
            @csrf

            <div class="input_container">
                <input type="text" name="uuid" hidden value="{{$banner->uuid}}">

                <select name="banner_section" id="" class="input_element_select" required>
                    <option value="home" @if($banner->section == 'home')selected @endif>Home</option>
                    <option value="project" @if($banner->section == 'project')selected @endif>Proyecto</option>
                    <option value="security" @if($banner->section == 'security')selected @endif>Seguridad</option>
                    <option value="virtual" @if($banner->section == 'virtual')selected @endif>Recorrido</option>
                    <option value="apartments" @if($banner->section == 'apartments')selected @endif>Departamentos
                    </option>
                    <option value="amenities" @if($banner->section == 'amenities')selected @endif>Amenidades</option>
                    <option value="map" @if($banner->section == 'map')selected @endif>Ubicación</option>
                    <option value="contact" @if($banner->section == 'contact')selected @endif>Contacto</option>
                    <option value="updates" @if($banner->section == 'updates')selected @endif>Avances</option>
                    <option value="media" @if($banner->section == 'media')selected @endif>Media</option>
                </select>
                @error('banner_content')
                <div class="error_container alert-danger mt-2">{{ $message }}</div>
                @enderror

                <input type="text" name="banner_content" placeholder="Contenido" class="input_element"
                    style="height: 50px; width:100%!important" required id="content_input" value="{{$banner->content}}">
                @error('banner_content')
                <div class="error_container alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </form>




        <div class="button_row" style="text-align: center">
            <div class="button"> <button class="btn btn-success saveButton">Guardar</button></div>

            @if($banner->active)
            <div class="button"> <button class="btn btn-warning deactivateButton">Desactivar</button></div>
            @else
            <div class="button"> <button class="btn btn-info activateButton">Activar</button></div>
            @endif
            <div class="button"> <button class="btn btn-danger deleteButton">Borrar</button></div>
        </div>

    </div>

</div>


<form action="{{route('changeStatusBanner')}}" method="POST" id="editPostStatus">
    @csrf
    <input type="text" name="uuid" hidden value="{{$banner->uuid}}">
    <input type="text" name="banner_visibility" hidden id="postVisibility">
    <input type="text" name="banner_status" hidden id="postStatus">
    <input type="text" name="banner_section" hidden value="{{$banner->section}}">
</form>




@endsection




@section('page_resources')

<script src="https://cdn.tiny.cloud/1/nzrm0s4rg9qzi1zgjblva8j9m0159iz68mv8xw7zm3rmmgxb/tinymce/5/tinymce.min.js"
    referrerpolicy="origin"></script>

<link rel="stylesheet" href="{{asset('css/dropzone.css')}}">
<script src="{{asset('js/dropzone.js')}}"></script>
{{-- <script src="{{asset('js/tinymce.min.js')}}"></script> --}}

<script>
    $(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });

});


$('.saveButton').click(function(){
if($('#content_input').val().length>0){
    showLoader()
}
$('#saveFormForm').submit();
});






$('.deleteImage').click(function(){
    confirmModal('¿Deseas borrar la imagen?',deleteImage);
})


$('.deactivateButton').click(function(){
    showLoader();
$('#postVisibility').val('true')
$('#postStatus').val('false')
$('#editPostStatus').submit();
})


$('.activateButton').click(function(){
    showLoader();
$('#postVisibility').val('true')
$('#postStatus').val('true')
$('#editPostStatus').submit();

})


$('.deleteButton').click(function(){
    confirmModal('¿Deseas borrar este banner?',deletePost);
})



function editPostStatusFn(){
    $('#confirmModal').modal('hide');
    showLoader();
$('#deleteImageForm').submit()
}


function deleteImage(){
    $('#confirmModal').modal('hide');
    showLoader();
$('#deleteImageForm').submit()
}

function deletePost(){
    $('#confirmModal').modal('hide');
    showLoader();
    $('#postVisibility').val('false')
$('#postStatus').val('false')
$('#editPostStatus').submit();


}


</script>
@endsection