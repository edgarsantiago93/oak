@extends('admin.mainLayout')



@section('page_content')



<div class="content_container">
    <div class="page_title mb-5">
        Lista de mensajes recibidos (Nuevos primero)
    </div>

    {{-- <div class="crud_row">
        <button class="btn btn-primary" onclick="window.location.href='{{route('admin.create_banner')}}'">
    Nuevo
    </button>
</div>
--}}


<div class="form_container_">


    <table>
        <thead>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Teléfono</th>
            <th>Status</th>
            <th></th>
        </thead>


        @foreach ($messages as $b)
        <tr>
            <td>{{$b->name}}</td>
            <td>{{$b->mail}}</td>
            <td>{{$b->phone}}</td>
            <td>{{!$b->active?'Atendido':'Por contestar'}}</td>
            <td><button class="btn btn-outline-info"
                    onclick="window.location.href='{{route('admin.read_message',['uuid'=>$b->uuid])}}'">Ver</button>
            </td>
        </tr>

        @endforeach

    </table>


</div>

{{-- <div class="input_container">
    <input type="text" name="user_name" placeholder="Nombre"
       class="input_element  @error('user_name') is-invalid @enderror" required
       value="{{$user->name}}">
@error('user_name')
<div class="error_container alert-danger">{{ $message }}</div>
@enderror
</div> --}}





</div>






@endsection






@section('page_resources')



<script>
    $(document).ready(function(){
      

});
</script>
@endsection