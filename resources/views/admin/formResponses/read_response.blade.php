@extends('admin.mainLayout')



@section('page_content')



<div class="content_container">


    <div class="page_title">
        Ver mensaje
    </div>


    <div class="form_container" style="margin-top: 30px;">

        <form action="{{route('editResponse')}}" method="POST" id="saveFormForm" class="create_form">
            @csrf
            <div class="input_container">
                <input type="text" name="uuid" hidden value="{{$userMessage->uuid}}">

                <select name="form_input_status" id="" class="input_element_select" required>
                    <option value="answered" @if(!$userMessage->active)selected @endif>Atendido</option>
                    <option value="tba" @if($userMessage->active)selected @endif>Por contestar</option>
                </select>


                <table class="mt-5">
                    <tr>
                        <td><b>Nombre</b></td>
                        <td>{{$userMessage->name}}</td>
                    </tr>
                    <tr>
                        <td><b>Correo</b></td>
                        <td>{{$userMessage->mail}}</td>
                    </tr>
                    <tr>
                        <td><b>Teléfono</b></td>
                        <td>{{$userMessage->phone}}</td>
                    </tr>
                    <tr>
                        <td><b>Mensaje</b></td>
                        <td>{{$userMessage->mensaje}}</td>
                    </tr>
                </table>


            </div>
        </form>




        <div class="button_row" style="text-align: center">
            <div class="button"> <button class="btn btn-success saveButton">Guardar</button></div>
            <div class="button"> <button class="btn btn-danger deleteButton">Borrar</button></div>
        </div>
    </div>
</div>




<form action="{{route('changeStatusResponse')}}" method="POST" id="editPostStatus">
    @csrf
    <input type="text" name="uuid" hidden value="{{$userMessage->uuid}}">
    <input type="text" name="response_visibility" hidden id="postVisibility">
</form>




@endsection




@section('page_resources')

<script src="https://cdn.tiny.cloud/1/nzrm0s4rg9qzi1zgjblva8j9m0159iz68mv8xw7zm3rmmgxb/tinymce/5/tinymce.min.js"
    referrerpolicy="origin"></script>

<link rel="stylesheet" href="{{asset('css/dropzone.css')}}">
<script src="{{asset('js/dropzone.js')}}"></script>
{{-- <script src="{{asset('js/tinymce.min.js')}}"></script> --}}

<script>
    $(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });

});


$('.saveButton').click(function(){
    showLoader()
$('#saveFormForm').submit();
});


$('.deleteButton').click(function(){
    confirmModal('¿Deseas borrar este mensaje?',deletePost);
})


function deletePost(){
    $('#confirmModal').modal('hide');
    showLoader();
    $('#postVisibility').val('false')
$('#postStatus').val('false')
$('#editPostStatus').submit();


}


</script>
@endsection