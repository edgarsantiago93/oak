@extends('admin.mainLayout')



@section('page_content')



<div class="content_container">


    <div class="page_title">
        Nuevo banner
    </div>


    <div class="form_container" style="margin-top: 30px;">

        <form action="{{route('saveBanner')}}" method="POST" id="saveFormForm" class="create_form">
            @csrf



            <div class="input_container">

                <select name="banner_section" id="" class="input_element_select" required>
                    <option value="home">Home</option>
                    <option value="project">Proyecto</option>
                    <option value="security">Seguridad</option>
                    <option value="virtual">Recorrido</option>
                    <option value="apartments">Departamentos</option>
                    <option value="amenities">Amenidades</option>
                    <option value="map">Ubicación</option>
                    <option value="contact">Contacto</option>
                    <option value="updates">Avances</option>
                    <option value="media">Media</option>
                </select>
                @error('banner_content')
                <div class="error_container alert-danger mt-2">{{ $message }}</div>
                @enderror


                <input type="text" name="banner_content" placeholder="Contenido" class="input_element"
                    style="height: 50px; width:100%!important" required id="content_input">
                @error('banner_content')
                <div class="error_container alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="" style="text-align: center">
                <div class="button"> <button class="btn btn-success saveButton" type="submit">Guardar</button></div>
            </div>

        </form>






    </div>




</div>




@endsection




@section('page_resources')


<script>
    $(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});



$('.saveButton').click(function(){

    if($('#content_input').val().length>0){
        showLoader()
    }

});




</script>
@endsection