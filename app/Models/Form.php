<?php

namespace App\Models;

use App\Models\InputField;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Form extends Model
{
    use HasFactory;


    public function inputs()
    {
        return $this->hasMany(InputField::class);
    }
}
