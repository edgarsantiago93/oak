<?php

namespace App\Models;

use Ramsey\Uuid\Uuid;
use App\Models\ImageUpload;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

//
class GeneralSetting extends Model
{
    use HasFactory;

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Uuid::uuid4();
        });
    }


    public function getFileId()
    {
        $file = ImageUpload::where('uuid', $this->content)->first();
        if ($file) {
            return $file->id;
        }
        return "0";
    }
    public function getFileLink()
    {
        $file = ImageUpload::where('uuid', $this->content)->first();
        if ($file) {
            return $file->filename;
        }
        return 'no-file';
    }
}
