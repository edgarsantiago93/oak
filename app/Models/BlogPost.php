<?php

namespace App\Models;

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BlogPost extends Model
{
    use HasFactory;
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Uuid::uuid4();
        });
    }



    public function getDate()
    {
        $dt= Carbon::parse($this->created_at)->timezone('America/Mexico_city');
        return $dt;
    }

    public function imgLink()
    {
        $img= ImageUpload::find($this->img);

        if (!$img) {
            return 'noImage.png';
        }
        return $img->filename;
    }
}
