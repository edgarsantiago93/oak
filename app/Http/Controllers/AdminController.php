<?php

namespace App\Http\Controllers;

use File;
use Artisan;
use App\Models\Banner;
use App\Models\BlogPost;
use App\Models\MediaPost;
use App\Models\InputField;
use App\Mail\FormInputMail;
use App\Models\BuildUpdate;
use App\Models\ImageUpload;
use Illuminate\Http\Request;
use App\Models\GeneralSetting;
use App\Models\FormInputResponse;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    public function saveForm(Request $request)
    {
        $name=InputField::where('type', 'name')->first();
        $mail=InputField::where('type', 'mail')->first();
        $phone=InputField::where('type', 'phone')->first();
        $msg=InputField::where('type', 'msg')->first();

        if (!$name) {
            $name=  new InputField();
            $name->type='name';
        }
        if (!$mail) {
            $mail=  new InputField();
            $mail->type='mail';
        }
        if (!$phone) {
            $phone = new InputField();
            $phone->type='phone';
        }
        if (!$msg) {
            $msg =  new InputField();
            $msg->type='msg';
        }

        $name->placeholder =    isset($request->name_placeholder)&&$request->name_placeholder != $name->placeholder?$request->name_placeholder:$name->placeholder;
        $mail->placeholder =    isset($request->mail_placeholder)&&$request->mail_placeholder != $mail->placeholder?$request->mail_placeholder:$mail->placeholder;
        $phone->placeholder =   isset($request->phone_placeholder)&&$request->phone_placeholder != $phone->placeholder?$request->phone_placeholder:$phone->placeholder;
        $msg->placeholder =     isset($request->msg_placeholder)&&$request->msg_placeholder  != $msg->msg_placeholder?$request->msg_placeholder:$msg->placeholder;


        $name->active=   isset($request->name_active)&&$request->name_active?true:false;
        $mail->active=   isset($request->mail_active)&&$request->mail_active?true:false;
        $phone->active=  isset($request->phone_active)&&$request->phone_active?true:false;
        $msg->active=    isset($request->msg_active)&&$request->msg_active?true:false;

        $name->required=    isset($request->name_required)&&$request->name_required?true:false;
        $mail->required=    isset($request->mail_required)&&$request->mail_required?true:false;
        $phone->required=   isset($request->phone_required)&&$request->phone_required?true:false;
        $msg->required=     isset($request->msg_required)&&$request->msg_required?true:false;


        $name->name= 'Nombre completo';
        $mail->name=  'Correo';
        $phone->name=  'Teléfono';
        $msg->name='Dejanos tu mensaje...';

        $name->save();
        $mail->save();
        $phone->save();
        $msg->save();
        return redirect()->back();
    }

    public function api_createBlogPost(Request $request)
    {
        $request->validate([
            'post_title' => 'required',
        ], $this->validationMessages());
        $blogPost =  new BlogPost();
        $blogPost->title  = $request->post_title;
        $blogPost->content  = $request->post_content;
        $img = ImageUpload::where('uuid', $request->post_photo)->first();
        $blogPost->img  = $img->id;
        $blogPost->save();
        return redirect()->back();
    }
    
    public function api_editBlog(Request $request)
    {
        $bpost =  BlogPost::where('uuid', $request->uuid)->first();
        $bpost->title  = $request->post_title;
        $bpost->content  = $request->post_content;

        if ($bpost->img != $request->post_photo) {
            $this->fileDestroyById($bpost->id);
        }
        $img = ImageUpload::where('uuid', $request->post_photo)->first();

        $bpost->img  = $img->id;
        $bpost->save();

        return redirect()->back();
    }
    public function api_changeStatusBlog(Request $request)
    {
        $bpost =  BlogPost::where('uuid', $request->uuid)->first();
        $bpost->active  = $request->post_status=='true'?1:0;
        $bpost->visible  = $request->post_visibility=='true'?1:0;
        $bpost->save();

        return redirect()->route('admin.blog');
    }

    public function saveSettings(Request $request)
    {
        $ytSet=GeneralSetting::where('type', 'youtube')->first();
        $wtSet=GeneralSetting::where('type', 'whatsapp')->first();
        $instaSet=GeneralSetting::where('type', 'instagram')->first();
        $fbSet=GeneralSetting::where('type', 'facebook')->first();
        $linkedSet=GeneralSetting::where('type', 'linkedin')->first();
        $spotifySet=GeneralSetting::where('type', 'spotify')->first();
        $trackCodSet=GeneralSetting::where('type', 'track_code')->first();
        $trackCodBodySet=GeneralSetting::where('type', 'track_code_body')->first();
        $mediaVideoSet=GeneralSetting::where('type', 'media_video')->first();
        $formMailSet=GeneralSetting::where('type', 'form_mail')->first();
        $initialVideo=GeneralSetting::where('type', 'initial_video')->first();
        $pdfFile=GeneralSetting::where('type', 'pdf_file')->first();
        $bookFile=GeneralSetting::where('type', 'book_file')->first();
        $phoneSet=GeneralSetting::where('type', 'phone')->first();


        if (!$phoneSet) {
            $phoneSet=  new GeneralSetting();
            $phoneSet->type='phone';
        }
        if (!$pdfFile) {
            $pdfFile=  new GeneralSetting();
            $pdfFile->type='pdf_file';
        }


        if (!$bookFile) {
            $bookFile=  new GeneralSetting();
            $bookFile->type='book_file';
        }
        if (!$ytSet) {
            $ytSet=  new GeneralSetting();
            $ytSet->type='youtube';
        }

        if (!$wtSet) {
            $wtSet=  new GeneralSetting();
            $wtSet->type='whatsapp';
        }
        if (!$instaSet) {
            $instaSet = new GeneralSetting();
            $instaSet->type='instagram';
        }
        if (!$fbSet) {
            $fbSet =  new GeneralSetting();
            $fbSet->type='facebook';
        }
        if (!$linkedSet) {
            $linkedSet= new GeneralSetting();
            $linkedSet->type='linkedin';
        }
        if (!$spotifySet) {
            $spotifySet = new GeneralSetting();
            $spotifySet->type='spotify';
        }
        if (!$trackCodSet) {
            $trackCodSet = new GeneralSetting();
            $trackCodSet->type='track_code';
        }
        if (!$trackCodBodySet) {
            $trackCodBodySet = new GeneralSetting();
            $trackCodBodySet->type='track_code_body';
        }
        if (!$mediaVideoSet) {
            $mediaVideoSet = new GeneralSetting();
            $mediaVideoSet->type='media_video';
        }
        if (!$formMailSet) {
            $formMailSet = new GeneralSetting();
            $formMailSet->type='form_mail';
        }
        if (!$initialVideo) {
            $initialVideo = new GeneralSetting();
            $initialVideo->type='initial_video';
        }


        $ytSet->content =     isset($request->youtube_link) && $request->youtube_link != $ytSet->content?$request->youtube_link:$ytSet->content;
        $wtSet->content =           isset($request->whatsapp_link) && $request->whatsapp_link!= $wtSet->content?$request->whatsapp_link:$wtSet->content;
        $instaSet->content =        isset($request->instagram_link) && $request->instagram_link!= $instaSet->content?$request->instagram_link:$instaSet->content;
        $fbSet->content =           isset($request->facebook_link) && $request->facebook_link!= $fbSet->content?$request->facebook_link:$fbSet->content;
        $linkedSet->content =       isset($request->linkedin_link) && $request->linkedin_link!= $linkedSet->content?$request->linkedin_link:$linkedSet->content;
        $spotifySet->content =      isset($request->spotify_link) && $request->spotify_link!= $spotifySet->content?$request->spotify_link:$spotifySet->content;
        $trackCodSet->content =     isset($request->track_link) && $request->track_link!= $trackCodSet->content?$request->track_link:$trackCodSet->content;
        $trackCodBodySet->content = isset($request->track_body_link) && $request->track_body_link!= $trackCodBodySet->content?$request->track_body_link:$trackCodBodySet->content;
        $mediaVideoSet->content =   isset($request->media_video) && $request->media_video!= $mediaVideoSet->content?$request->media_video:$mediaVideoSet->content;
        $formMailSet->content =     isset($request->form_mail) && $request->form_mail!= $formMailSet->content?$request->form_mail:$formMailSet->content;
        $initialVideo->content =    isset($request->initial_video) && $request->initial_video!= $initialVideo->content?$request->initial_video:$initialVideo->content;
        $pdfFile->content =         isset($request->pdf_file) && $request->pdf_file!= $pdfFile->content?$request->pdf_file:$pdfFile->content;
        $bookFile->content =        isset($request->book_file) && $request->book_file!= $bookFile->content?$request->book_file:$bookFile->content;
        $phoneSet->content =        isset($request->phone_link) && $request->phone_link!= $phoneSet->content?$request->phone_link:$phoneSet->content;





        $ytSet->active =        isset($request->youtube_visible)&&$request->youtube_visible?true:false;
        $wtSet->active =        isset($request->whatsapp_visible)&&$request->whatsapp_visible?true:false;
        $instaSet->active =     isset($request->instagram_visible)&&$request->instagram_visible?true:false;
        $fbSet->active =        isset($request->facebook_visible)&&$request->facebook_visible?true:false;
        $linkedSet->active =    isset($request->linkedin_visible)&&$request->linkedin_visible?true:false;
        $spotifySet->active =   isset($request->spotify_visible)&&$request->spotify_visible?true:false;
        $trackCodSet->active =  isset($request->track_visible)&&$request->track_visible?true:false;
        $trackCodBodySet->active =  isset($request->track_body_visible)&&$request->track_body_visible?true:false;
        $initialVideo->active = isset($request->initial_video_visible)&&$request->initial_video_visible?true:false;
        $phoneSet->active = isset($request->phone_visible)&&$request->phone_visible?true:false;



        $ytSet->save();
        $wtSet->save();
        $instaSet->save();
        $fbSet->save();
        $linkedSet->save();
        $spotifySet->save();
        $trackCodSet->save();
        $trackCodBodySet->save();
        $mediaVideoSet->save();
        $formMailSet->save();
        $initialVideo->save();
        $initialVideo->save();
        $pdfFile->save();
        $bookFile->save();
        $phoneSet->save();
        return redirect()->back();
    }


    public function validationMessages()
    {
        return  $messages = [
            'uuid.required' => 'La información es incompleta',
            'user_name.required' => 'El nombre es necesario.',
            'user_name.min' => 'Por favor escribe un nombre válido.',
            'user_address.min' => 'Por favor escribe un dirección válida.',
            'user_address.required' => 'La dirección es necesaria.',
            'user_last_name.required' => 'Los apellidos es necesario.',
            'user_last_name.min' => 'Por favor escribe apellidos válidos.',
            'user_mail.required' => 'El nombre es necesario.',
            'user_phone.required' => 'El teléfono es necesario.',
            'user_zip.required' => 'El código postal es necesario.',
            'user_state.required' => 'El estado es necesario.',
            'user_city.required' => 'La ciudad es necesaria.',
            'terms_conditions.required' => 'Debes aceptar los términos y condiciones.',
            'user_mail.email' => 'Por favor escribe un correo válido.',
            'user_mail.unique' => 'El correo ya está registrado. Si quieres registrar un ticket nuevo ponte en contacto con CORREO@CORREO.COM',
            'user_mail.max' => 'El correo no debe exceder los 255 caracteres.',
            'user_phone.numeric' => 'Por favor escribe un teléfono válido (solo números).',
            'user_phone.digits_between' => 'El teléfono debe tener mínimo 8 y máximo 10 dígitos.',
            'user_phone.max' => 'El teléfono no debe exceder los 10 dígitos.',
            'user_zip.min' => 'El código postal debe tener mínimo 4 dígitos.',
            'user_password.min' => 'El password postal debe tener mínimo 8 caracteres.',
        ];
    }



    // Route methods
    // Route methods
    // Route methods
    // Route methods
    // Route methods

    // Blog
    public function formsRoute()
    {
        $inputs= InputField::where('visible', 1)->get();
        $mailSetting =GeneralSetting::where('type', 'form_mail')->first();
        return view('admin.forms')->with('inputs', $inputs)->with('mailToSend', $mailSetting);
    }


    // Form Responses
    public function messagesRoute()
    {
        $messages= FormInputResponse::where('visible', 1)->orderByDesc('created_at')->get();
        return view('admin.formResponses.list_responses')->with('messages', $messages);
    }

    // public function createBlog()
    // {
    //     return view('admin.blog.create_blog');
    // }

    public function readMessage($uuid)
    {
        $message= FormInputResponse::where('uuid', $uuid)->first();
        return view('admin.formResponses.read_response')->with('userMessage', $message);
    }
    
    // Blog
    public function blogRoute()
    {
        $posts= BlogPost::where('visible', 1)->get();
        return view('admin.blog.list_blog')->with('posts', $posts);
    }

    public function createBlog()
    {
        return view('admin.blog.create_blog');
    }

    public function readBlog($uuid)
    {
        $post= BlogPost::where('uuid', $uuid)->first();
        return view('admin.blog.read_blog')->with('post', $post);
    }

    // Media
    public function mediaRoute()
    {
        $media= MediaPost::where('visible', 1)->get();
        $videoSetting =GeneralSetting::where('type', 'media_video')->first();
        return view('admin.media.list_media')->with('media', $media)->with('video', $videoSetting);
    }

    public function createMedia()
    {
        return view('admin.media.create_media');
    }

    public function readMedia($uuid)
    {
        $media= MediaPost::where('uuid', $uuid)->first();
        return view('admin.media.read_media')->with('media', $media);
    }


    // Update
    public function updatesRoute()
    {
        $updates= BuildUpdate::where('visible', 1)->get();
        return view('admin.constructionUpdates.list_updates')->with('updates', $updates);
    }

    public function createUpdate()
    {
        return view('admin.constructionUpdates.create_update');
    }

    public function readUpdate($uuid)
    {
        $update= BuildUpdate::where('uuid', $uuid)->first();
        return view('admin.constructionUpdates.read_update')->with('update', $update);
    }



    // Bannners
    public function bannersRoute()
    {
        $banners= Banner::where('visible', 1)->get();
        return view('admin.banners.list_banners')->with('banners', $banners);
    }

    public function createBanner()
    {
        return view('admin.banners.create_banner');
    }

    public function readBanner($uuid)
    {
        $banner= Banner::where('uuid', $uuid)->first();
    
        return view('admin.banners.read_banner')->with('banner', $banner);
    }


    // Settings, images and files


    public function settingsRoute()
    {
        $settings= GeneralSetting::where('visible', 1)->get();
        return view('admin.settings')->with('settings', $settings);
    }


    public function storeImage(Request $request)
    {
        $regPhoto =  ImageUpload::where('uuid', $photo)->first();
        if ($regPhoto) {
            File::move(public_path('images/'.$regPhoto->filename), public_path('images/'.$regPhoto->filename));
            $regPhoto->is_temp=false;
            $regPhoto->registered_item_id=$regItem->id;
            $regPhoto->save();
        }
    }

    public function fileDestroyById($id)
    {
        $img = ImageUpload::find($id);
        if ($img) {
            $path=public_path().'/images/'.$img->filename;
            if (file_exists($path)) {
                unlink($path);
            }
            $img->delete();
        }
    }



    // API Media


    public function api_createMedia(Request $request)
    {
        $request->validate([
            'post_title' => 'required',
        ], $this->validationMessages());
        $mediaPost =  new MediaPost();
        $mediaPost->title  = $request->post_title;
        $mediaPost->content  = $request->post_content;
        $img = ImageUpload::where('uuid', $request->post_photo)->first();
        $mediaPost->img  = $img->id;
        $mediaPost->save();
        return redirect()->back();
    }
    
    public function api_editMedia(Request $request)
    {
        $mediaPost =  MediaPost::where('uuid', $request->uuid)->first();
        $mediaPost->title  = $request->post_title;
        $mediaPost->content  = $request->post_content;

        if ($mediaPost->img != $request->post_photo) {
            $this->fileDestroyById($bpost->id);
        }
        $img = ImageUpload::where('uuid', $request->post_photo)->first();

        $mediaPost->img  = $img->id;
        $mediaPost->save();

        return redirect()->back();
    }
    
   
    public function api_changeStatusMedia(Request $request)
    {
        $bpost =  MediaPost::where('uuid', $request->uuid)->first();
        $bpost->active  = $request->post_status=='true'?1:0;
        $bpost->visible  = $request->post_visibility=='true'?1:0;
        $bpost->save();

        return redirect()->route('admin.media');
    }
    
    
    // API update
 
    public function api_createUpdate(Request $request)
    {
        $request->validate([
            'post_title' => 'required',
        ], $this->validationMessages());
        $buildPost =  new BuildUpdate();
        $buildPost->title  = $request->post_title;
        $buildPost->content  = $request->post_content;
        $img = ImageUpload::where('uuid', $request->post_photo)->first();
        $buildPost->img  = $img->id;
        $buildPost->save();
        return redirect()->back();
    }
    
    public function api_editUpdate(Request $request)
    {
        $buildPost =  BuildUpdate::where('uuid', $request->uuid)->first();
        $buildPost->title  = $request->post_title;
        $buildPost->content  = $request->post_content;

        if ($buildPost->img != $request->post_photo) {
            $this->fileDestroyById($buildPost->id);
        }
        $img = ImageUpload::where('uuid', $request->post_photo)->first();

        $buildPost->img  = $img->id;
        $buildPost->save();

        return redirect()->back();
    }
    public function api_changeStatusUpdate(Request $request)
    {
        $buildPost =  BuildUpdate::where('uuid', $request->uuid)->first();
        $buildPost->active  = $request->post_status=='true'?1:0;
        $buildPost->visible  = $request->post_visibility=='true'?1:0;
        $buildPost->save();

        return redirect()->route('admin.updates');
    }

    
    // API banner
 
    public function api_createBanner(Request $request)
    {
        $request->validate([
            'banner_content' => 'required',
        ], $this->validationMessages());
        $existing = Banner::where('visible', 1)->where('active', 1)->where('section', $request->banner_section)->get();
        foreach ($existing as $e) {
            $e->active =0;
            $e->save();
        }

        $banner = new Banner();
        $banner->section =$request->banner_section;
        $banner->content =$request->banner_content;
        $banner->save();
        
        return redirect()->route('admin.banners');
    }
    
    public function api_editBanner(Request $request)
    {
        $banner =  Banner::where('uuid', $request->uuid)->first();

        $existing = Banner::where('visible', 1)->where('active', 1)->where('section', $request->banner_section)->get();
        foreach ($existing as $e) {
            $e->active =0;
            $e->save();
        }
        $banner->section =$request->banner_section;
        $banner->content =$request->banner_content;
        $banner->save();

        return redirect()->back();
    }


    public function api_changeStatusBanner(Request $request)
    {
        $existing = Banner::where('visible', 1)->where('active', 1)->where('section', $request->banner_section)->get();
        foreach ($existing as $e) {
            $e->active =0;
            $e->save();
        }

        $banner =  Banner::where('uuid', $request->uuid)->first();
        $banner->active  = $request->banner_status=='true'?1:0;
        $banner->visible  = $request->banner_visibility=='true'?1:0;
        $banner->save();

        return redirect()->route('admin.banners');
    }



    // Response
     
    public function api_editResponse(Request $request)
    {
        $fi =  FormInputResponse::where('uuid', $request->uuid)->first();
        $fi->active =$request->form_input_status =='tba'?1:0;
        $fi->save();
        return redirect()->back();
    }

    public function api_changeStatusResponse(Request $request)
    {
        $fi =  FormInputResponse::where('uuid', $request->uuid)->first();
        $fi->visible  = $request->response_visibility=='true'?1:0;
        $fi->save();

        return redirect()->route('admin.messages');
    }


    // form input
    public function api_saveFormInput(Request $request)
    {
        $fi = new FormInputResponse();
        $fi->name = $request->input_name;
        $fi->mail = $request->input_mail;
        $fi->phone = $request->input_phone;
        $fi->mensaje=$request->input_message;
        $fi->save();

        if ($request->brochure) {
            $fi->brochure =true;
        }

        // Mail::to(['edgar@santiagoguerrero.mx','esg.827@gmail.com'])->send(new FormInputMail($fi));
        Mail::to(['building.ad1@gmail.com','contactos@amatedigital.mx'])->send(new FormInputMail($fi));
        if ($request->brochure) {
            return redirect('/')->with('download', 'yes');
        }
        // return redirect()->route('thankyou')
        return redirect('/#gracias')->with('success', 'Tu mensaje ha sido enviado con éxito');
    }
    public function u()
    {
        Artisan::call('up');
    }
}
