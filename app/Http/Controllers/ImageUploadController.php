<?php

namespace App\Http\Controllers;

use App\Models\ImageUpload;
use Illuminate\Http\Request;
use App\Models\GeneralSetting;

class ImageUploadController extends Controller
{
    public function fileStore(Request $request)
    {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images'), $imageName);
        
        $imageUpload = new ImageUpload();
        $imageUpload->filename = $imageName;
        $imageUpload->save();
        return response()->json(['success'=>$imageName,'imUuid'=>$imageUpload->uuid]);
    }


    public function fileDestroy(Request $request)
    {
        $filename =  $request->get('filename');
        ImageUpload::where('filename', $filename)->delete();
        $path=public_path().'/images/'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;
    }


    public function fileDestroyById(Request $request)
    {
        $img = ImageUpload::find($request->img_id);
        if (!$img) {
            return redirect()->back();
        }

        $path=public_path().'/images/'.$img->filename;
        if (file_exists($path)) {
            unlink($path);
        }
        $img->delete();

        if (isset($request->deletion_type)) {
            $g = GeneralSetting::where('type', $request->deletion_type)->first();
            if ($g) {
                $g->content ='';
                $g->save();
            }
        }


        return redirect()->back();
    }
}
