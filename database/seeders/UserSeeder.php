<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array(
            0 =>
            array(
                'id' => 1,
                'uuid' => '93dca5c2-4a17-475c-b1f0-6a36fb72d612',
                'name' => 'Admin',
                'email' => 'admin@oak58.com',
                'email_verified_at' => null,
                'password' => '$2y$10$Zhzva//4/tepR5/Pmuvl6ehQfoq3nRmDFFx3euCvl66qeszNkveDK',
                'remember_token' => null,
                'created_at' => '2020-12-09 18:06:15',
                'updated_at' => '2020-12-09 18:21:26',
            ),
        ));
    }
}
